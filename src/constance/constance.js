export const apiUrl = "http://localhost:2005/api/"; // <<<<< same back end  >>>>>>
export const APP_TITLE = "U-Shin";

export const NETWORK_CONNECTION_MESSAGE =
  "Cannot connect to server, Please try again.";
export const NETWORK_TIMEOUT_MESSAGE =
  "A network timeout has occurred, Please try again.";
export const UPLOAD_PHOTO_FAIL_MESSAGE =
  "An error has occurred. The photo was unable to upload.";
export const NOT_CONNECT_NETWORK = "NOT_CONNECT_NETWORK";

export const key = {
  LOGIN_PASSED: `LOGIN_PASSED`,
  API_KEY: `API_KEY`,
  USER_LV: `USER_LV`,
  USER_ID: "USER_ID",
  USER_EMP: "USER_EMP",
};

export const YES = "YES";
export const NO = "NO";
export const OK = "OK";
export const NOK = "NOK";

export const server = {
  // Login/Register
  LOGIN_URL: `auth/login`,
  URL_REGIST: `auth/regist`,

  // Childpart
  GET_CHILD_PART: `ChildPartList`,

  //DO
  GET_DO: `get_DO`,


  // Employee
  getEmployee: `getEmployee`,
  getRole: `getRole`,
  insertEmployee: `insertEmployee`,
  insertEmployeeRole: `insertEmployeeRole`,
  SUBMIT_EMPLOYEE : `saveEmployee`,

  //PastList
  insertPartList: `insertPartList`,
  getPartList: `getPartList`,

  // Machine
  getMasterMachine: `getMasterMachine`,
  insertMachine: `insertMachine`,

  //POMaster
  getMasterPOStatus: `getMasterPOStatus`,
  getProd: `getProductionResultRefined`,
  ProdData: `productionResult`,  
  GetProductionResultLatest : `getProductionResultLatest` ,
  Post_insertEmployee: `insertEmployee`,

  //ReceiveMat
  GET_MATCHINGID: `getMatchingIdState`,
  CHECK_MATCHINGID: `checkMatchingId`,
  SUBMIT: `submitAllDataNew`,
  CLEAR: `clearData`,
  SPLIT: `populateMOSplit`,
  REMOVE_RECEIVE: `deleteLastScannedMat`,
  GET_SCAN_CHILDPART : `GetScanChildPartLeft`,


  //Tracecibility
  Tracecibility: `checkDo`,
  GET_CUSTOMERSPN: `GetCustomersPN`,
  GET_DUEDATE : `GetDueDate`,
  CHECK_DODUEDATE : `checkDoDuedate`,


  //EditDO
  GET_ALLDO : `getAllDONo`,
  GET_ALLPO : `getAllPONo`,
  DELETEPO : `deletePO`,


  //Shopper
  // GET_POSTATE: `getPoState`,
  // CHECK_MATCHINGPO : `checkMatchingPO`,
  // CHECK_MATCHINGPO_MAT : `checkMatchingPOMat`,
  // ADD_MACHINE: `addMachine`,
  // ADD_MATERIAL: `addMat`,
  // CHECK_MAT_NEW: `checkMatchingPOMatNew`,
  // SUBMIT_ALLPO: `insertAllPOPage`,
  // CANCEL_PO: `clearDataPOPage`,
  // REMOVE_PO: `deletePOLastScannedMaterial`,
  GET_MATCHINGIDSTATE: `getMatchingIdStateShopperStore`,
  GET_MO: `getMOFromReceive`,



  CHECKMATCHINGID_SHOPPER: `checkMatchingIdShopperStore`,
  CHECKMATCHINGID: `insertAllPOPage`,

  ADD_MACHINE: `addMachineShopperStore`,
  SUBMIT_DO: `submitDO`,
  SUBMIT_MO: `submitAllDataNewShopperStore`,
  SUBMIT_MATERIAL: `shopperPOSubmitAll`,

  CANCEL_DATA_MO: `clearDataShopperStore`,
  CANCRL_DATA_MATERIAL: `clearDataPOPageShopperStore`,

  REMOVE_MATERIAL: `removeLastScannedMaterial`,

  MO_SPLIT: `populateMOSplitShopperStore`,

  //WIP
  Workinprocess: `popageclose`,
  GET_SHOWDETIAL : `ShowDetialWIP`,

  // Master 
  // Add_groupPart : `master/add_groupPartlist`, 
  // GET_groupPart: `master/get_groupPart`, 
  // GET_Part: `master/get_PartList`, 
  // GET_masterMC: `master/get_masterMC`, 
  // GET_masterPOstatus: `master/GET_masterPOstatus`, 
  SUBMIT_REASON: `inputReasonForStockLost`,

  //Dashboard
  getSummarizeOutput: `getSummarizeOutput`,
  getMaterialInlineStock: `getMaterialInlineStock`,
  MO_print: `MO_print`,
  insert_MoDataPrint: `insert_MoDataPrint`,
  getMoDataPrint: `getMoDataPrint`,
  getChildPartPrint: `getChildPartPrint`,
  selectState_Reprint : `selectState_Reprint`,
  getMoDataRePrint: `getMoDataRePrint`,
  msg_update: `msg_update`,
  insertMoDataLog : `insertMoDataLog`,


  // Upload
  RECEIVECSVFILE_OPUCUS: `receiveOpucas`,
  RECEIVECSVFILE_BOM: `receiveBOM`,


  //ControlMaterial
  GET_ALLOWANCE : `getPartAllowance`,
  SET_ALLOWANCE : `setPartAllowance`, 
};
