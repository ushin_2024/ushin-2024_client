import React, { Component } from "react";
import { httpClient } from "../../../utils/HttpClient";
import { Link } from "react-router-dom";
import { OK, server } from "../../../constance/constance";
import Swal from "sweetalert2";
import "./MaterialControl.css";

export default class MaterialControl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      materials: [],
    };
  }

  componentDidMount = async () => {
    let serverState = await httpClient.get(server.GET_ALLOWANCE);
    let currentState = this.state;
    currentState.materials = serverState.data;
    this.setState(currentState);
  };

  handleChange = (index, e) => {
    const { checked } = e.target;
    let allowStr = checked ? "True" : "False";

    // Create a copy of the materials array
    let updatedMaterials = [...this.state.materials];
    // Update the Allow value of the specific row
    updatedMaterials[index].Allow = allowStr;

    // Update the state with the new materials array
    this.setState({ materials: updatedMaterials });
  };

  Submit = async (event) => {
    const res = await httpClient.post(server.SET_ALLOWANCE, this.state.materials);
    console.log(res.data)
    switch (res.data) {
      case "OK":
        Swal.fire({
          title: "Success",
          text: "Material Control Success",
          icon: "success",
          timer: 1500,
        });
        break;
    
      default:
        Swal.fire({
          title: "Error",
          text: "Material Can't Control !",
          icon: "error",
          timer: 1500,
        });
        break;
    }
  };

  CheckBool = (string) => {
    return string === "True";
  };

  RenderPartAllowanceData = (row, index) => {
    return (
      <tr key={index}>
        <td>{row["ChildPartList.ChildPartNo"]}</td>
        <td>{row["ChildPartList.ChildPartName"]}</td>
        <td>
          <input
            type="checkbox"
            onChange={(e) => this.handleChange(index, e)}
            checked={this.CheckBool(row.Allow)}
          />
        </td>
      </tr>
    );
  };

  render() {
    return (
      <div className="wrapper">
        <div className="content-wrapper">
          <div className="content-header"></div>
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header card-header-MatCon "> Material Control </div>
                    <div className="card-body">
                      <div className="row">
                        <table className="table table-MatCon table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>ChildPart No</th>
                              <th>ChildPart Name</th>
                              <th>Allow</th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.materials.map((row, index) =>
                              this.RenderPartAllowanceData(row, index)
                            )}
                          </tbody>
                        </table>
                      </div>
                      <div className="text-center mt-3">
                        <button
                          onClick={this.Submit}
                          type="submit"
                          className="btn btn-success btn-custom"
                        >
                          <i className="fas fa-download"></i> Submit
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
