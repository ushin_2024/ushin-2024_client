import React, { Component } from "react";
import "./home.css";
import { Link } from "react-router-dom";

import {
  FcHome,
  // FcInTransit,
  // FcDataSheet,
  FcManager,
  FcConferenceCall,
  FcList,
  FcDeleteColumn,
} from "react-icons/fc";
// import { CiBoxList } from "react-icons/ci";

export default class home extends Component {
  render() {
    return (
      <>
        <div className="wrapper" style={{ overflowX: "hidden" }}>
          <div className="content-wrapper">
            <div className="content-header"></div>
            <section className="content">
            <div className="row">
              <div className="col">
                <div className="card card-info card-outline">
                  <div className="card-header">
                    <h3 className="card-title">
                      {" "}
                      <p style={{ fontSize: "25px" }}> Record Data</p>
                    </h3>
                  </div>
                  <div className="card-body">
                    <div className="row">
                      {/* <div className="col">
                        <div
                          className="small-box"
                          style={{ backgroundColor: "#64CCEF" }}
                        >
                          <div className="inner">
                            <h4> Receive Material </h4>
                            <p> รับ Material จาก store</p>
                          </div>
                          <div className="icon">
                            <FcHome />
                          </div>
                          <Link to="/ReceiveMat" className="small-box-footer">
                            CLICK <i className="fas fa-arrow-circle-right" />
                          </Link>
                        </div>
                      </div> */}

                      <div className="col-4">
                        <div
                          className="small-box"
                          style={{ backgroundColor: "#C3ACE6", marginTop:"10px", marginLeft:"10px" }}
                        >
                          <div className="inner">
                            <h4> Add Employee for Line </h4>
                            <p>เพิ่ม Employee เพื่อจับคู่กับ Line ในการทำงาน </p>
                          </div>
                          <div className="icon">
                            <FcConferenceCall />
                          </div>
                          <Link to="/EmployeeScan" className="small-box-footer">
                            CLICK <i className="fas fa-arrow-circle-right" />
                          </Link>
                        </div>
                      </div>
                      
                      <div className="col-4">
                        <div
                          className="small-box"
                          style={{ backgroundColor: "#64CCEF", marginTop:"10px"}}
                        >
                          <div className="inner">
                            <h4> Shopper </h4>
                            <p style={{ fontSize: "15px" }}>
                              Scan MO, Material เพื่อจับ Production Line {" "}
                            </p>
                          </div>
                          <div className="icon">
                            <FcManager />
                          </div>
                          <Link to="/ShopperRecive" className="small-box-footer">
                            CLICK <i className="fas fa-arrow-circle-right" />
                          </Link>
                        </div>
                      </div>

                      <div className="col-4">
                        <div
                          className="small-box"
                          style={{ backgroundColor: "#60D2BC", marginTop:"10px", marginRight:"10px" }}
                        >
                          <div className="inner">
                            <h4> WIP/FG Output </h4>
                            <p>Scan WIP/FG Output </p>
                          </div>
                          <div className="icon">
                            <FcList />
                          </div>
                          <Link to="/WIP_output" className="small-box-footer">
                            CLICK <i className="fas fa-arrow-circle-right" />
                          </Link>
                        </div>
                      </div>
                      <div className="col"></div>
                    </div>
                  </div>
                </div>
                <div className="card card-info card-outline">
                  {/* <div className="card-header">
                    <h3 className="card-title">
                      {" "}
                      <p style={{ fontSize: "25px" }}> Input data </p>
                    </h3>
                  </div> */}
                  <div className="card-body">
                    <div className="row">
                      <div className="col-4">
                        <div
                          className="small-box"
                          style={{ backgroundColor: "#FA7C46", marginTop:"10px", marginLeft:"10px"}}
                        >
                          <div className="inner">
                            <h4> Lose/NG Material </h4>
                            <p> Material สูญหาย หรือ NG</p>
                          </div>
                          <div className="icon">
                            <FcDeleteColumn />
                          </div>
                          <Link to="/Losepage" className="small-box-footer">
                            CLICK <i className="fas fa-arrow-circle-right" />
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* <div className="col-6">
                <div className="card card-primary card-outline">
                  <div className="card-header">
                    <h3 className="card-title">
                      {" "}
                      <b> Data</b>{" "}
                    </h3>
                  </div>
                  <div className="card-body">
                    <div className="row"></div>
                    <div className="row">
                      <div className="col-md-6 col-6">
                        <div
                          className="small-box"
                          style={{ backgroundColor: "#E8DABF" }}
                        >
                          <div className="inner">
                            <h4>Delivery Order</h4>
                            <p>Check DO Detail </p>
                          </div>
                          <div className="icon">
                            <FcInTransit />
                          </div>

                          <Link to="/DeliveryData" className="small-box-footer">
                            CLICK <i className="fas fa-arrow-circle-right" />
                          </Link>
                        </div>
                      </div>
                      <div className="col-md-6 col-6">
                        <div
                          className="small-box"
                          style={{ backgroundColor: "#EBC75E" }}
                        >
                          <div className="inner">
                            <h4> Traceability </h4>
                            <p>Check Data</p>
                          </div>
                          <div className="icon">
                            <FcDataSheet />
                          </div>
                          <Link to="/Traceability" className="small-box-footer">
                            CLICK <i className="fas fa-arrow-circle-right" />
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}
