import React, { Component } from "react";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import { key, server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import DataTable from "react-data-table-component";
import moment from "moment";
import { Table } from "antd";

class TraceabilityTest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scannedDo: "",
      savedDO: "",
      DOTable: [],
      MCTable: [],
      LotTable: [],
      data: [],
      dataSub: [],
      dataTB1: [],
    };
    this.handleChangeDo = this.handleChangeDo.bind(this);
  }
  handleChangeDo = async (event) => {
    this.setState({ scannedDo: event.target.value });
  };
  handleSubmit = async (event) => {
    let sendData = await httpClient.post(server.Tracecibility, {
      data: { scannedDo: String(this.state.scannedDo) },
    });

    // console.log(sendData.data);
    let currentState = this.state;
    // console.log(sendData.data)

    if (
      currentState.DOTable.length > 1 ||
      currentState.MCTable.length > 1 ||
      currentState.LotTable.length > 1
    ) {
      currentState.DOTable[0] = sendData.data["DO_PO_TABLE"];
      currentState.MCTable[0] = sendData.data["MC_TABLE"];
      currentState.LotTable[0] = sendData.data["LOT_TABLE"];
    } else {
      currentState.DOTable.push(sendData.data["DO_PO_TABLE"]);
      currentState.MCTable.push(sendData.data["MC_TABLE"]);
      currentState.LotTable.push(sendData.data["LOT_TABLE"]);
    }

    console.log(currentState);
    if (
      currentState.DOTable[0] === undefined ||
      currentState.MCTable[0] === undefined ||
      currentState.LotTable[0] === undefined
    ) {
      console.log("error");
      Swal.fire({
        title: "Error!",
        text: "No Data",
        icon: "error",
        // confirmButtonText: 'Cool'
      });
    } else {
      if (
        currentState.DOTable[0].length === 0 &&
        currentState.MCTable[0].length === 0 &&
        currentState.LotTable[0] === 0
      ) {
        console.log("error");
        Swal.fire({
          title: "Error!",
          text: "Incorrectly Scaned",
          icon: "error",
          // confirmButtonText: 'Cool'
        });
      } else {
        console.log("success");
        Swal.fire({
          title: "Success!",
          text: "Data Complete",
          icon: "success",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    }
    this.setState({
      savedDO: currentState.scannedDo,
      dataTB1: sendData.data["LOT_TABLE"],
    });
    this.setState({ scannedDo: "" });
    console.log(currentState);
    console.log(this.state.dataTB1);
  };

  GetTable1 = async (event) => {
    const datanew = [];
    for (let i = 0; i < this.state.dataTB1.length; i++) {
      const item = this.state.dataTB1[i];
      datanew.push({
        key: item.i,
        id: item["POs.POId"],
        PONo: item["POs.PONo"],
        PartName: item.PartName,
        DueDate: item.DueDate,
        DeliveryDueDate: item.DeliveryDueDate,
        DueQty: item.DueQty,
        CustomersPN: item.CustomersPN,
        ChildPartName: item["POs.ProductionData.ChildPartList.ChildPartName"],
        ChildPartNo: item["POs.ProductionData.ChildPartList.ChildPartNo"],
        MachineName: item["POs.ProductionData.Machine.MachineName"],
      });
    }
    await this.setState({ data: datanew });
    console.log(datanew);
  };
  getTable2 = async () => {
    const columns = [
      { title: "ID", dataIndex: "id", key: "id" },
      { title: "mc1", dataIndex: "ChildPartName", key: "ChildPartName" },
      { title: "mc2", dataIndex: "ChildPartNo", key: "ChildPartNo" },
      { title: "mc3", dataIndex: "MachineName", key: "MachineName" },
    ];
    const data2 = [];
    for (let i = 0; i < this.state.dataTB1.length; i++) {
      const item2 = this.state.dataTB1[i];
      data2.push({
        key: i,
        id: item2["POs.POId"],
        ChildPartName: item2["POs.ProductionData.ChildPartList.ChildPartName"],
        ChildPartNo: item2["POs.ProductionData.ChildPartList.ChildPartNo"],
        MachineName: item2["POs.ProductionData.Machine.MachineName"],
      });
    }
    await this.setState({ dataSub: data2 });
  };
  DoStuff = () => {
    if (this.state.DOTable[0] != null) {
      let DO = "";
      try {
        let DO_PATTERN = /N\w+-\w+-\w+|N\w+-\w+|N\w+/g;
        const tag = this.state.savedDO;
        const arr = [...tag.matchAll(DO_PATTERN)];
        DO = arr[0][0];
      } catch (e) {
        console.log(e);
      }
      return `DONo : ${DO}`;
    }
  };

  render() {
    const columns = [
      { title: "ID", dataIndex: "id", key: "id" },
      { title: "PONo", dataIndex: "PONo", key: "PONo" },
      { title: "PartName", dataIndex: "PartName", key: "PartName" },
      { title: "DueDate", dataIndex: "DueDate", key: "DueDate" },
      {
        title: "DeliveryDueDate",
        dataIndex: "DeliveryDueDate",
        key: "DeliveryDueDate",
      },
      { title: "DueQty", dataIndex: "DueQty", key: "DueQty" },
      { title: "CustomersPN", dataIndex: "CustomersPN", key: "CustomersPN" },
      {
        title: "ChildPartName",
        dataIndex: "ChildPartName",
        key: "ChildPartName",
      },
      { title: "ChildPartNo", dataIndex: "ChildPartNo", key: "ChildPartNo" },
      { title: "MachineName", dataIndex: "MachineName", key: "MachineName" },
    ];
    const expandedSub = (record) => {
      const columns = [
        { title: "ID", dataIndex: "id", key: "id" },
        { title: "mc1", dataIndex: "ChildPartName", key: "ChildPartName" },
        { title: "mc2", dataIndex: "ChildPartNo", key: "ChildPartNo" },
        { title: "mc3", dataIndex: "MachineName", key: "MachineName" },
      ];
      console.log("record", record);
      const data = [];

      const item = this.state.dataSub[record.key];
      data.push({
        key: record.key,
        // id: item.id,
        ChildPartName: item.ChildPartName,
        ChildPartNo: item.ChildPartNo,
        MachineName: item.MachineName,
      });
    };

    return (
      <>
        <div className="wrapper" style={{ overflowX: "hidden" }}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1 >Traceability</h1>
                  </div>
                </div>
              </div>

              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-6">
                            <div>
                              <form
                                onSubmit={this.handleSubmit}
                                style={{
                                  display: "inline-block",
                                  width: 500,
                                  marginTop: "10px",
                                  marginLeft: "20px",
                                  marginRight: "20px",
                                  marginBottom: "20px",
                                }}
                              >
                                <label>Scan DO </label>
                                <input
                                  type="text"
                                  value={this.state.scannedDo}
                                  onChange={this.handleChangeDo}
                                  className="form-control"
                                  placeholder="input DO"
                                />
                              </form>
                            </div>
                          </div>
                          <button
                            onClick={async (event) => {
                              event.preventDefault();
                              await this.handleSubmit();
                              await this.GetTable1();
                            }}
                            style={{
                              margin: "35px 0px 20px 10px",
                              fontSize: "1 rem",
                              color: "white",
                              fontWeight: "bold",
                              width: "100px",
                            }}
                            className="btn btn-primary"
                          >
                            Search
                          </button>
                        </div>
                      </div>
                    </div>

                    <div>
                      {(() => {
                        if (
                          this.state.DOTable != null &&
                          this.state.MCTable != null &&
                          this.state.LotTable != null
                        ) {
                          return (
                            <>
                              <div className="card">
                                <div className="card-header">
                                  <h3 className="card-title">
                                    {this.DoStuff()}
                                  </h3>
                                </div>
                                <Table
                                  className="components-table-demo-nested"
                                  columns={columns}
                                  expandedRowRender={(record) =>
                                    expandedSub(record)
                                  }
                                  rowKey={(record) => record.id}
                                  dataSource={this.state.data}
                                />
                              </div>
                              ReactDOM.render(
                              <this.handleSubmit />,
                              document.getElementById("container"));
                            </>
                          );
                        } else {
                          return <div>Loading...</div>;
                        }
                      })()}{" "}
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}

export default TraceabilityTest;
