import React, { Component } from "react";
import { server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import DataTable from "react-data-table-component";
import moment from "moment";
import Swal from "sweetalert2";

class Tab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scannedDo: "",
      savedDO: "",
      DOTable: [],
      MCTable: [],
      LotTable: [],
      SelectableCustomersPN: [],
      SelectableDueDate: [],
    };
    this.handleChangeDo = this.handleChangeDo.bind(this);
  }

  componentDidMount = async () => {
    let customerPNs = await httpClient.get(server.GET_CUSTOMERSPN);
    let currentState = this.state;
    currentState.SelectableCustomersPN = customerPNs.data;
    this.setState(currentState);
  };

  handleChangeDo = async (event) => {
    this.setState({ scannedDo: event.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      let sendData = await httpClient.post(server.Tracecibility, {
        data: { scannedDo: String(this.state.scannedDo) },
      });
      let currentState = this.state;

      switch (sendData.data) {
        case "No Data":
          Swal.fire({
            title: "Error!",
            text: "Incorrectly Scaned",
            icon: "error",
          });
          break;

        default:
          if (
            currentState.DOTable.length > 1 ||
            currentState.MCTable.length > 1 ||
            currentState.LotTable.length > 1
          ) {
            currentState.DOTable[0] = sendData.data["DO_PO_TABLE"];
            currentState.MCTable[0] = sendData.data["MC_TABLE"];
            currentState.LotTable[0] = sendData.data["LOT_TABLE"];
          } else {
            currentState.DOTable.push(sendData.data["DO_PO_TABLE"]);
            currentState.MCTable.push(sendData.data["MC_TABLE"]);
            currentState.LotTable.push(sendData.data["LOT_TABLE"]);
          }

          if (
            currentState.DOTable[0].length === 0 ||
            currentState.MCTable[0].length === 0 ||
            currentState.LotTable[0].length === 0 ||
            currentState.scannedDo.length === 0
          ) {
            console.log("error");
            Swal.fire({
              title: "Warning!",
              text: "Some data is missing !",
              icon: "warning",
              // confirmButtonText: 'Cool'
            });
          } else {
            console.log("success");
            Swal.fire({
              title: "Success!",
              text: "Data Complete",
              icon: "success",
              showConfirmButton: false,
              timer: 1500,
            });
          }
          break;
      }

      console.log(currentState);

      this.setState({ savedDO: currentState.scannedDo });
      this.setState({ scannedDo: "" });
    } catch (error) {
      console.log("Error");
    }
  };

  DoStuff = () => {
    if (this.state.DOTable[0] != null) {
      let DO = "";
      try {
        let DO_PATTERN = /N\w+-\w+-\w+|N\w+-\w+|N\w+/g;
        const tag = this.state.savedDO;
        const arr = [...tag.matchAll(DO_PATTERN)];
        DO = arr[0][0];
      } catch (e) {
        console.log(e);
      }
      return `DONo : ${DO}`;
    }
  };

  RenderCustomersPN = (customersPN) => {
    return (
      <>
        <option value={`${customersPN}`}>{`${customersPN}`}</option>
      </>
    );
  };

  RenderDueDate = (dueDate) => {
    return (
      <>
        <option value={`${dueDate}`}>{`${dueDate}`}</option>
      </>
    );
  };

  OnSelectCustomersPN = async (e) => {
    if (e.target.value !== "CustomersPN") {
      let dueDate = await httpClient.post(server.GET_DUEDATE, {
        data: e.target.value,
      });
      let currentState = this.state;
      currentState.SelectableDueDate = dueDate.data;
      this.setState(currentState);
    }
  };

  CustomersDueDateSearch = async (event) => {
    let selectedCustomersPN = document.getElementById("customersPN");
    let selectedDueDate = document.getElementById("dueDate");
    if (
      selectedCustomersPN.value === "true" ||
      selectedDueDate.value === "true"
    ) {
      return;
    }

    event.preventDefault();
    let sendData = await httpClient.post(server.CHECK_DODUEDATE, {
      CustomersPN: selectedCustomersPN.value,
      DueDate: selectedDueDate.value,
    });

    let currentState = this.state;

    if (
      currentState.DOTable.length > 1 ||
      currentState.MCTable.length > 1 ||
      currentState.LotTable.length > 1
    ) {
      currentState.DOTable[0] = sendData.data["DO_PO_TABLE"];
      currentState.MCTable[0] = sendData.data["MC_TABLE"];
      currentState.LotTable[0] = sendData.data["LOT_TABLE"];
    } else {
      currentState.DOTable.push(sendData.data["DO_PO_TABLE"]);
      currentState.MCTable.push(sendData.data["MC_TABLE"]);
      currentState.LotTable.push(sendData.data["LOT_TABLE"]);
    }

    if (
      currentState.DOTable[0].length === 0 ||
      currentState.MCTable[0].length === 0 ||
      currentState.LotTable[0].length === 0 ||
      currentState.scannedDo.length === 0
    ) {
      console.log("pumperror1");
    } else {
      console.log("pumperror2");
    }

    this.setState({ savedDO: currentState.scannedDo });
    this.setState({ scannedDo: "" });
  };

  render() {
    const DO_TABLE = [
      {
        name: "PONo",
        selector: (row) => row["POs.PONo"],
      },
      {
        name: "CustomersPN",
        selector: (row) => row.CustomersPN,
      },
      {
        name: "PartName",
        selector: (row) => row.PartName,
      },
      {
        name: "Delivery Due Date",
        selector: (row) => row.DeliveryDueDate,
        format: (row) => moment(row.DueDate).format("DD-MM-YYYY"),
      },
      {
        name: "Production Due Date",
        selector: (row) => row.DueDate,
        format: (row) => moment(row.DueDate).format("DD-MM-YYYY"),
      },

      {
        name: "Registered",
        selector: (row) => row.createdAt,
        format: (row) => moment(row.createdAt).format("DD-MM-YYYY, h:mm A"),
      },
    ];

    const MC_TABLE = [
      {
        name: "MONo",
        selector: (row) => row["FROM_MONo"],
      },
      {
        name: "ProcessName",
        selector: (row) => row["ChildPartList.ProcessName"],
      },
      {
        name: "ChildPartNo",
        selector: (row) => row["ChildPartList.ChildPartNo"],
      },
      {
        name: "ChildPartName",
        selector: (row) => row["ChildPartList.ChildPartName"],
      },
      {
        name: "ChildPartLot",
        selector: (row) => row["ChildPartLot"],
      },
      {
        name: "Registered",
        selector: (row) => row.createdAt,
        format: (row) => moment(row.createdAt).format("DD-MM-YYYY, h:mm A"),
      },
    ];

    const LOT_TABLE = [
      {
        name: "PONo",
        selector: (row) => row["POs.PONo"],
      },
      {
        name: "ProcessName",
        selector: (row) => row["MachineName"],
      },
      {
        name: "EmployeeId",
        selector: (row) => row["EmployeeId"],
      },
      {
        name: "Registered",
        selector: (row) => row.MatchedDataDate,
        format: (row) => moment(row.MatchedDataDate).format("DD-MM-YYYY, h:mm A"),
      },
    ];

    return (
      <>
        <div className="wrapper" style={{ overflowX: "hidden" }}>
          <div className="content-wrapper">
            <section className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1 style={{ fontFamily: "poppins" }}>Traceability</h1>
                  </div>
                </div>
              </div>
              <div className="card">
                <div className="card-body">
                  <div className="row">
                    <div className="col-6">
                      <form
                        onSubmit={this.handleSubmit}
                        style={{
                          display: "inline-block",
                          width: 500,
                          marginTop: "10px",
                          marginLeft: "20px",
                          marginRight: "20px",
                          marginBottom: "20px",
                        }}
                      >
                        <label> Scan FG Tag </label>
                        <input
                          type="text"
                          value={this.state.scannedDo}
                          onChange={this.handleChangeDo}
                          className="form-control"
                          placeholder="input FG Tag"
                        />
                      </form>

                      <button
                        onClick={this.handleSubmit}
                        style={{
                          margin: "15px 0px 20px 10px",
                          fontSize: "1 rem",
                          color: "white",
                          fontWeight: "bold",
                          width: "100px",
                        }}
                        className="btn btn-primary"
                      >
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="container-fluid" style={{ fontSize: "15px" }}>
                {/* Tab card  */}
                <div>
                  {" "}
                  <br />
                  <div className="col-12">
                    <div className="card card-primary card-outline card-outline-tabs">
                      <div className="card-header p-0 border-bottom-0">
                        <div>
                          <h4 style={{ margin: "8px 10px 10px 5px" }}>
                            {this.DoStuff()}
                          </h4>
                        </div>
                        <ul
                          className="nav nav-tabs"
                          id="custom-tabs-four-tab"
                          role="tablist"
                        >
                          <li className="nav-item">
                            <a
                              className="nav-link active"
                              id="custom-tabs-four-home-tab"
                              data-toggle="pill"
                              href="#custom-tabs-four-home"
                              role="tab"
                              aria-controls="custom-tabs-four-home"
                              aria-selected="true"
                              onClick={(e) => {
                                this.setState({ edit_display: "none" });
                              }}
                            >
                              DO Detial
                            </a>
                          </li>
                          <li className="nav-item">
                            <a
                              className="nav-link"
                              id="custom-tabs-four-profile-tab"
                              data-toggle="pill"
                              href="#custom-tabs-four-profile"
                              role="tab"
                              aria-controls="custom-tabs-four-profile"
                              aria-selected="false"
                              onClick={(e) => {
                                this.setState({ edit_display: "none" });
                              }}
                            >
                              {" "}
                              Material Lot{" "}
                            </a>
                          </li>
                          <li className="nav-item">
                            <a
                              className="nav-link"
                              id="custom-tabs-four-messages-tab"
                              data-toggle="pill"
                              href="#custom-tabs-four-messages"
                              role="tab"
                              aria-controls="custom-tabs-four-messages"
                              aria-selected="false"
                              onClick={(e) => {
                                this.setState({ edit_display: "none" });
                              }}
                            >
                              Line
                            </a>
                          </li>
                          <div style={{ display: "flex", marginBottom:'0.5rem'}}> 
                            {" "}
                            {/* Search */}
                            <div className="col" style={{ display: "flex", alignItems: "center" }}>
                              <select
                                name="customersPN"
                                className="form-control"
                                id="customersPN"
                                style={{ marginRight: "10px",  minWidth: "fit-content" }}
                                onChange={(e) => this.OnSelectCustomersPN(e)}
                              >
                                <option disabled selected value>
                                  CustomersPN
                                </option>
                                {this.state.SelectableCustomersPN.map((i) =>
                                  this.RenderCustomersPN(i)
                                )}
                              </select>

                              <select 
                                name="dueDate" 
                                className="form-control" 
                                id="dueDate" 
                                style={{ marginRight: "10px", minWidth: "fit-content" 
                                }}>
                                  
                                <option disabled selected value>
                                  DueDate
                                </option>
                                {this.state.SelectableDueDate.map((i) =>
                                  this.RenderDueDate(i)
                                )}
                              </select>

                              <input
                                type="button"
                                value="Search"
                                className="btn btn-info"
                                onClick={(e) => this.CustomersDueDateSearch(e)}
                              ></input>
                            </div>
                          </div>
                        </ul>
                      </div>

                    {/* Show Data */}
                      <div className="card-body">
                        <div
                          className="tab-content"
                          id="custom-tabs-four-tabContent"
                        >
                          <div
                            className="tab-pane fade show active"
                            id="custom-tabs-four-home"
                            role="tabpanel"
                            aria-labelledby="custom-tabs-four-home-tab"
                          >
                            <div className="card-body table-responsive p-0">
                              <DataTable
                                style={{}}
                                columns={DO_TABLE}
                                data={this.state.DOTable[0]}
                              />
                            </div>
                          </div>
                          <div
                            className="tab-pane fade"
                            id="custom-tabs-four-profile"
                            role="tabpanel"
                            aria-labelledby="custom-tabs-four-profile-tab"
                          >
                            <DataTable
                              columns={MC_TABLE}
                              data={this.state.MCTable[0]}
                            />
                          </div>
                          <div
                            className="tab-pane fade"
                            id="custom-tabs-four-messages"
                            role="tabpanel"
                            aria-labelledby="custom-tabs-four-messages-tab"
                          >
                            <DataTable
                              columns={LOT_TABLE}
                              data={this.state.LotTable[0]}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}

export default Tab;
