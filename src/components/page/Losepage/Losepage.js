import React, { Component } from "react";
import { Link } from "react-router-dom";
import { OK, server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import Swal from "sweetalert2";

export default class Losepage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scannedMat: "",
      selectReason: "",
    };
    this.handleChangeScanMat = this.handleChangeScanMat.bind(this);
    this.handleChangeReason = this.handleChangeReason.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeScanMat = async (event) => {
    this.setState({ scannedMat: event.target.value });
  };
  handleChangeReason = async (event) => {
    this.setState({ selectReason: event.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    let currentState = this.state;
    console.log(currentState);
    let test = await httpClient.post(server.SUBMIT_REASON, currentState);
    if (test.data === "notok") {
      Swal.fire({
        title: "Error!",
        text: "No Data",
        icon: "error",
      });
    } else if (test.data.status === "OK") {
      Swal.fire({
        title: "Success!",
        text: "Save success",
        icon: "success",
        
      });
      this.setState({ scannedMat: "" });
      this.setState({ selectReason: "" });
      this.setState(test.data);
    }
  };

  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
          <div className="content-header"></div>
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header"  style={{ fontSize: "30px"}} >Lose/NG Material</div>
                    <div className="card-body">
                      <div className="row"  style={{marginTop:"10px", marginLeft:"20px", marginRight:"20px"}}>
                        <div className="col-5">
                          <div>
                            <form onSubmit={this.handleSubmit}>
                              <label style={{ display: "block" }}>
                                Scan Material
                                <input
                                  type="text"
                                  value={this.state.scannedMat}
                                  onChange={this.handleChangeScanMat}
                                  className="form-control"
                                  placeholder="input Material"
                                />
                              </label>
                              <input
                                type="submit"
                                value="Submit"
                                style={{ visibility: "hidden" }}
                              ></input>
                            </form>
                          </div>
                        </div>

                        <div className="col-5">
                          <div>
                            <label style={{ display: "block" }}>
                              {" "}
                              Select Reason
                              <select
                                className="form-control"
                                value={this.state.selectReason}
                                onChange={this.handleChangeReason}
                              >
                                <option> Select Reason </option>
                                <option value="Lose">Lose</option>
                                <option value="NG">NG</option>
                              </select>
                            </label>
                          </div>
                        </div>
                        <div className="col">
                          <button
                            onClick={this.handleSubmit}
                            className="btn btn-success"
                            style={{
                              margin: "23px 0px 0px 50px",
                            }}
                          >
                            {" "}
                            Submit
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}
