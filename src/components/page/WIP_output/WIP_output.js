import React, { Component } from "react";
import { Link } from "react-router-dom";
import { server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import moment from "moment";
import Swal from "sweetalert2";

export default class WIP_output extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scannedPONo: "",
      scannedMC: "",
      showMC: "",
      showPONo: [],
      scanedDO: "",
      ShowData: [],
      showTable: false,
      tableData: [],
      getDOList: [],
      totalCard: 0,
      storePO: [],
    };

    this.handleChangePoNo = this.handleChangePoNo.bind(this);
    this.handleChangeMachine = this.handleChangeMachine.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangePoNo = async (event) => {
    this.setState({ scannedPONo: event.target.value });
    this.setState({ scanedDO: event.target.value });
  };
  handleChangeMachine = async (event) => {
    this.setState({ scannedMC: event.target.value });
  };

  componentDidMount = async () => {
    await this.getDO();
  };

 
  getDO = async () => {
    let DOdata = await httpClient.get(server.GET_DO);
    await this.setState({ getDOList: DOdata.data });
  };



  renderTB() {
    return this.state.tableData.map((row, index) => {
      const isExist = row.Exist === "Exist";
      const isChecked = this.state.showPONo.some(po => po.PONo === row["POs.PONo"] && po.checked);
      const bgColor = isExist
        ? { backgroundColor: "#40C2A8", color: "white", fontSize: "14px", borderRadius: "4px", padding: "8px", boxShadow: "0 2px 5px rgba(0, 0, 0, 0.2)" }
        : isChecked
          ? { backgroundColor: "#F8B07B", color: "white", fontSize: "14px", borderRadius: "4px", padding: "8px", boxShadow: "0 2px 5px rgba(0, 0, 0, 0.2)" }
            : {};
  
      return (
        <tr key={index}>
          <td style={bgColor}>{row.Exist === "None" ? "None" : isExist ? "OK" : row.Exist}</td>
          <td style={bgColor}>{row["POs.CardNo"]}</td>
          <td style={bgColor}>{row["POs.PONo"]}</td>
          <td style={bgColor}>{row.MachineName}</td>
          <td style={bgColor}>{row.createdAt === "None" ? "None" : moment(row.createdAt).format("YYYY-MM-DD HH:mm:ss")}</td>
        </tr>
      );
    });
  }
  
  

  handleSubmit = async (event) => {
    const doFound = this.DoStuff().trim();
    if (event.key === "Enter") {
      event.preventDefault();
      let currentState = this.state;
      if (doFound !== "DO not found") {
        try {
          let response = await httpClient.post(server.GET_SHOWDETIAL,{Data: doFound, MC: this.state.showMC});
          console.log(response)
          let data = response.data
          currentState.ShowData = data[0]
          currentState.totalCard = data[1]
          this.setState(currentState)
          console.log(data[0])
          const found = data[0].map((item) => item.DONo.trim());
          const uniqueFound = [...new Set(found)]; //Change Arr to Set for remove duplicate data
          console.log("Data DO All SQL", uniqueFound);

          const isFound = found.some((DONo) => DONo === doFound);
          console.log("isFound",isFound);

          this.findTotalCard(doFound);

          if (isFound) {
            this.setState({
              showTable: true,
              tableData: data[0],
            });
          } else {
            this.setState({
              showTable: false,
              tableData: [],
            });
          }
        } catch (error) {
          this.setState({
            showTable: false,
            tableData: [],
          });
          console.error("Error fetching data:", error);
        }
      }
      let match = event.target.value.match(/D.+/g);
      let dup = false;
      for (let i = 0; i < this.state.showPONo.length; i++) {
        if (
          String(this.state.showPONo[i].PONo).trim() === String(match).trim()
        ) {
          dup = true;
        }
      }
      // console.log(dup);

      // console.log(currentState.scannedPONo);

      //  const scannedPONo =
      // console.log(this.state.scannedPONo);
      // if (this.state.scannedPONo === "") {
      //   console.log("error");
      //   console.log(this.state.scannedPONo);
      //   await Swal.fire({
      //     title: "Error",
      //     text: "No Data",
      //     icon: "error",
      //   });
      // }

      // // if (this.state.scannedPONo === "") {
      //   Swal.fire({
      //     title: "Error!",
      //     text: "PO is null",
      //     icon: "error",
      //   });
      // }

      if (
        this.state.scannedPONo.length !== 0 &&
        dup === false &&
        match !== null
      ) {
        currentState.showPONo.push({
          PONo: String(match).trim(),
          checked: true,
        });
        // console.log(currentState.showPONo);
      } else if (this.state.scannedPONo === "") {
        Swal.fire({
          title: "Error!",
          text: "PO is null",
          icon: "error",
        });
      } else if (dup) {
        Swal.fire({
          title: "Error!",
          text: "PO is duplicated.",
          icon: "error",
        });
      } else if (match === null) {
        Swal.fire({
          title: "Error!",
          text: "PO is null",
          icon: "error",
        });
      }

      this.setState({ scannedPONo: "" });

      // currentState.showPONo.push({ PONo: String(match).trim(), checked: true });
      // this.setState({ scannedPONo: "" });
    }
  };

  findTotalCard = (DONo) => {
    const { getDOList } = this.state;
    const matchedDO = getDOList.find((Item) => Item.DONo.trim() === DONo);
    // console.log(matchedDO)
    if (matchedDO) {
      this.setState({ totalCard: matchedDO.TotalCard });
    } else {
      this.setState({ totalCard: 0 });
    }
  };

  SubmitMC = async (event) => {
    if (event.key === "Enter") {
      this.setState({ showMC: event.target.value });
      this.setState({ scannedMC: "" });
      const inputs = document.querySelectorAll("input");
      const currentIndex = Array.from(inputs).findIndex(
        (input) => input === document.activeElement
      );
      const nextIndex = currentIndex + 1;
      if (inputs[nextIndex]) {
        inputs[nextIndex].focus();
      }
    }
  };

  // handleChange = (row, e) => {
  //   const { checked } = e.target;
  //   row.checked = checked;
  // };

  handleChange = (row, e) => {
    const { checked } = e.target;
    this.setState(prevState => ({
      showPONo: prevState.showPONo.map(po =>
        po.PONo === row.PONo ? { ...po, checked } : po
      )
    }));
  };

  ShowPOTable(row) {
  if (!this.state.storePO.includes(row.PONo)) {
    this.setState(prevState => ({
      storePO: [...prevState.storePO, row.PONo]
    }));
  }

  return (
    <>
      <br></br>
      <input
        type="checkbox"
        onChange={(e) => this.handleChange(row, e)}
        defaultChecked={row.checked}
        style={{ transform: "scale(2)", marginRight: "20px" }}
      />
      <label style={{ fontSize: "25px" }}>{row.PONo}</label>
    </>
  );
}

  
  Submit = async (event) => {
    event.preventDefault();
    let serverState = await httpClient.post(server.Workinprocess, this.state);
    let status = serverState.data;
    console.log(status);
    switch (status) {
      case "No Data":
        Swal.fire({
          title: "Error!",
          text: "No Data ScanPO Again",
          icon: "error",
          timer: 1500,
        });
        break;
      case "INVALID_SCAN_DO_PO":
        Swal.fire({
          title: "Error!",
          text: "Invalid Scan QR Code PO",
          icon: "error",
          timer: 1500,
        });
        break;
      case "CARD_HAS_BEEN_SCANNED":
        Swal.fire({
          title: "Error!",
          text: "Card has been scanned",
          icon: "error",
          timer: 1500,
        });
        break;
      case "MachineNotFoundInDatabase":
        Swal.fire({
          title: "Error!",
          text: "PO is null!",
          icon: "error",
          timer: 1500,
        });
        break;
      case "No ChildPart found on ProductionData":
        Swal.fire({
          title: "Error!",
          text: "ChildPart not found",
          icon: "error",
          timer: 1500,
        });
        break;
      case "StockIsNotEnough":
        Swal.fire({
          title: "Error!",
          text: "Material Not Enough",
          icon: "error",
          timer: 1500,
        });
        break;
      default:
        Swal.fire({
          title: "Success!",
          text: "Save Success",
          icon: "success",
          timer: 1500,
        });
        break;
    }
    this.setState({
      scannedPONo: "",
      scannedMC: "",
      showMC: "",
      showPONo: [],
      scanedDO: "",
      ShowData: [],
      showTable: false,
      tableData: [],
      getDOList: [],
      totalCard: 0,
    });
  };

  Cancel = async (event) => {
    this.setState({
      scannedPONo: "",
      scannedMC: "",
      showMC: "",
      showPONo: [],
      scanedDO: "",
      ShowData: [],
      showTable: false,
      tableData: [],
      getDOList: [],
      totalCard: 0,
    });
    window.location.reload();
  };

  DoStuff = () => {
    const savedDO = this.state.scanedDO;
    const DO_PATTERN = /N\w+-\w+-\w+|N\w+-\w+|N\w+/g;
    const arr = savedDO.match(DO_PATTERN);
    return arr && arr.length ? ` ${arr[0]}` : "DO not found";
  };

  handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      const inputs = document.querySelectorAll("input");
      const currentIndex = Array.from(inputs).findIndex(
        (input) => input === document.activeElement
      );
      const nextIndex = currentIndex + 1;
      if (inputs[nextIndex]) {
        inputs[nextIndex].focus();
      } else if (event.key === "Enter") {
        event.preventDefault();
        // Simulate a click on the button
        const saveButton = document.getElementById("saveButton");
        if (saveButton) {
          saveButton.click();
        }
      }
    }
  };

  render() {
    const {
      scannedMC,
      scannedPONo,
      showMC,
      showPONo,
      scanedDO,
      showTable,
      tableData,
      totalCard,
    } = this.state;
    return (
      <>
        <div div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header"></div>
            <section className="content">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-12">
                    <div className="card card-primary card-outline">
                      <div className="card-header">
                        <h3 className="card-title" style={{ fontSize: "30px" }}>
                          WIP/FG Output
                        </h3>
                      </div>
                      <div className="card-body">
                        <div
                          className="row"
                          style={{
                            marginTop: "10px",
                            marginLeft: "20px",
                            marginRight: "20px",
                          }}
                        >
                          <div className="col-6">
                            {/* <from onSubmit={this.handleSubmit}> */}
                            <div className="form-group">
                              <label htmlFor="exampleInputNO">Scan Line</label>
                              <input
                                type="text"
                                value={this.state.scannedMC}
                                onChange={this.handleChangeMachine}
                                onKeyDown={(e) => this.SubmitMC(e)}
                                className="form-control"
                                placeholder="Input Line"
                              />
                            </div>
                            {/* </from> */}
                          </div>

                          <div className="col-6">
                            <div className="form-group">
                              <label htmlFor="exampleInputNO">Scan PO</label>
                              <input
                                type="text"
                                value={this.state.scannedPONo}
                                onChange={this.handleChangePoNo}
                                onKeyDown={(e) => this.handleSubmit(e)}
                                className="form-control"
                                placeholder="Input PO"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="text-muted mt-3"></div>
                      </div>
                    </div>
                    <div className="row">
                      {/* Show Po Wait for all follow DO  */}

                      <div className="col-6">
                        <div className="card">
                          <div
                            className="card-body"
                            style={{ marginLeft: "30px" }}
                          >
                            {/* Show data scaned PO */}
                            <div
                              style={{ fontSize: "25px", marginTop: "10px" }}
                            >
                              {this.state.showMC}
                            </div>
                            {this.state.showPONo.map((row) =>
                              this.ShowPOTable(row)
                            )}

                            <div
                              style={{
                                display: "flex",
                                justifyContent: "flex-end",
                                margin: "15px 5px 5px ",
                              }}
                            >
                              <button
                                id="saveButton"
                                onClick={this.Submit}
                                type="submit"
                                className="btn btn-success"
                                style={{ marginRight: "10px" }}
                              >
                                {" "}
                                Submit All
                              </button>

                              <button
                                onClick={this.Cancel}
                                className="btn btn-danger"
                              >
                                {" "}
                                Cancel{" "}
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Show Data Detial*/}
                      <div className="col-6">
                        <div className="card">
                          <div className="card-header">
                            <h5 className="card-title" style={{}}>
                              DONo : {this.DoStuff()}
                              {this.DoStuff() !== "DO not found" && (
                                <> TotalCard: {totalCard} </>
                              )}
                            </h5>
                          </div>
                          <div className="card-body">
                            {" "}
                            {showTable && (
                              <table className="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>Successful</th>
                                    <th>CardNo</th>
                                    <th>PONo</th>
                                    <th>LineName</th>
                                    <th>CreatedAt</th>
                                  </tr>
                                </thead>
                                <tbody style={{ fontFamily: "sans-serif" }}>
                                  {this.renderTB()}
                                </tbody>
                              </table>
                            )}
                            {/* <table className="table table-bordered">
                              <thead>
                                <tr>
                                  <th>CardNo</th>
                                  <th>CardNo</th>
                                  <th>PONo</th>
                                  <th>CreatedAt</th>
                                </tr>
                              </thead>
                              <tbody style={{ fontFamily: "sans-serif" }}>
                                {this.renderTB()}
                              </tbody>
                            </table> */}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}
