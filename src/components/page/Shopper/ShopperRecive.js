import React, { Component } from "react";
import { Link } from "react-router-dom";
import ReactDOM from "react-dom";
import {  server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import Swal from "sweetalert2";
import debounce from "debounce";
import { RotatingLines } from "react-loader-spinner";

export default class ShopperRecive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      savedMOValues: [],
      MOValue: "",
      scannedMO: "",
      scanText: "",
      storedValidMat: [],
      row: 0,
      DO: "",
      status: "",
      BOM_LEFTOVER: [],
      storedMachine: [],
      availableReceiveMO: [],
      selectedMOPO: "",
      storedMachineGuide: [],
      savedDO: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeMat = this.handleChangeMat.bind(this);
    this.handleChangeScannedMO = this.handleChangeScannedMO.bind(this);
    this.handleChangeSelectMO = this.handleChangeSelectMO.bind(this);
  }
  handleChangeMat(event) {
    this.setState({ scanText: event.target.value });
  }
  handleChangeScannedMO(event) {
    this.setState({ scannedMO: event.target.value });
  }

  handleChangeDo = async (event) => {
    this.setState({ DO: event.target.value });
  };

  componentDidMount = async () => {
    let serverState = await httpClient.get(server.GET_MATCHINGIDSTATE);
    this.setState(serverState.data);

    let getAvailableMO = await httpClient.get(server.GET_MO);
    let data = getAvailableMO.data;
    this.setState({ availableReceiveMO: data });
    this.setState({ status: "" });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    let debounce_function = debounce(async function (ShopperStore) {
      Swal.fire({
        title: "Uploading...",
        html: '<div id="loader" style="display: flex; justify-content: center; align-items: center; height: 100px; overflow: hidden;"></div>',
        allowOutsideClick: false,
        showConfirmButton: false,
        didOpen: () => {
          const loader = document.getElementById("loader");
          ReactDOM.render(
            <RotatingLines color="#00BFFF" height={100} width={100} />,
            loader
          );
        },
      });

      let currentState = ShopperStore.state;
      if (currentState.MOValue === "" || currentState.scanText === "") {
        Swal.fire({
          title: "Error!",
          text: "Please fill in all fields",
          icon: "error",
          timer: 1500,
        });
        return;
      }
      // console.log(currentState.storedValidMat);
      let test = await httpClient.post(
        server.CHECKMATCHINGID_SHOPPER,
        currentState
      );

      ShopperStore.setState(test.data);
      ShopperStore.setState({ scanText: "" });

      // console.log(ShopperStore.state.status);
      switch (test.data.status) {
        case "MATTERIAL_PATTERN_ERROR":
          Swal.fire({
            title: "Error!",
            text: "Pattern Material Error!",
            icon: "error",
          });
          break;
        case "MATERIAL_DUPLICATE_ERROR":
          Swal.fire({
            title: "Error!",
            text: "Material Duplicate!",
            icon: "error",
          });
          break;

        default:
          Swal.close();
          break;
      }

      ShopperStore.setState({ status: "" });
    }, 300);

    debounce_function(this);
  };

  handleSubmitPO = async (event) => {
    event.preventDefault();

    let sendScannedMc = await httpClient.post(server.ADD_MACHINE, this.state);
    this.setState(sendScannedMc.data);
    this.setState({ MC: "" });
  };

  // Submit อันบน MO Material
  SubmitAll = async () => {
    let currentState = this.state;

    if (
      currentState.savedMOValues.length === 0 ||
      currentState.storedValidMat.length === 0
    ) {
      Swal.fire({
        title: "Error!",
        text: "Please fill in all fields",
        icon: "error",
        timer: 1500,
      });
      return;
    }

    let test = await httpClient.post(server.SUBMIT_MO, currentState);

    switch (test.data.status) {
      case "No Data":
        Swal.fire({
          title: "Error!",
          text: "No Data",
          icon: "error",
          timer: 2000,
        });
        break;
      case "ExceededMatFound":
        Swal.fire({
          title: "Error!",
          text: "Over Material",
          icon: "error",
          timer: 3000,
        });
        break;
      case "Error":
        Swal.fire({
          title: "Error!",
          text: "Error 404 not found",
          icon: "error",
          timer: 1500,
        });
        break;
      case undefined:
        Swal.fire({
          title: "Error!",
          text: "Data not found",
          icon: "error",
          timer: 3000,
        });
        break;
      default:
        Swal.fire({
          title: "Success",
          text: "Save success",
          icon: "success",
          timer: 1500,
        });
        break;
    }
    this.setState(test.data);
    setTimeout(function () {
      window.location.reload();
    }, 2500);
  };

  CancelData = async () => {
    let clearedData = await httpClient.get(server.CANCEL_DATA_MO);
    console.log(clearedData);
    this.setState(clearedData.data);
    window.location.reload();
  };
  CancelDataPO = async () => {
    let clearedData = await httpClient.get(server.CANCRL_DATA_MATERIAL);
    this.setState(clearedData.data);
    window.location.reload();
  };

  TestFunction(row) {
    // console.log(row);
    return (
      <>
        <label>{row["no"]}&nbsp;</label>
        <label>{row["partNo"]}&nbsp;</label>
        <label>{row["polotlabel"]}&nbsp;</label>
        <label>{row["qty"]}</label>
        <br></br>
      </>
    );
  }

  scanMOSplit = async (event) => {
    if (event.key === "Enter") {
      // console.log(event.target.value)
      let serverState = await httpClient.post(server.MO_SPLIT, {
        data: event.target.value,
      });
      this.setState(serverState.data);
      console.log(this.state);
      this.setState({ scannedMO: "" });
    }
  };

  ShowDropDown(eachMO) {
    // console.log(eachMO)
    let mo = eachMO;
    let fragment = <option value={`${mo}`}>{mo}</option>;

    if (mo === this.state.MOValue) {
      fragment = (
        <option value={`${mo}`} selected="selected">
          {mo}
        </option>
      );
    }

    return <>{fragment}</>;
  }

  ShowDropDownReceive(eachMOPO) {
    let mo = eachMOPO;
    return (
      <>
        <option value={`${mo}-po`}>{mo}</option>
      </>
    );
  }

  handleValue(row, event) {
    // console.log(row)
    row.Process.value = event.target.value;
    // console.log(this.state)
  }

  ShowMCTable(row) {
    // console.log(row)
    return (
      <>
        <label>{row.Process.ProcessName}</label>
        <input
          className="form-control"
          type="text"
          onChange={(e) => this.handleValue(row, e)}
        ></input>
        <br></br>
      </>
    );
  }

  handleChangeSelectMO(event) {
    this.setState({ MOValue: event.target.value });
  }
  handleChangeSelectMOPO(event) {
    this.setState({ selectedMOPO: event.target.value });
  }

  SubmitDO = async (event) => {
    if (event.key === "Enter") {
      let currentState = this.state;
      currentState.savedDO = event.target.value;
      let serverState = await httpClient.post(server.SUBMIT_DO, currentState);
      this.setState(serverState.data);
      console.log(serverState);
    }
  };

  // Submit อันล่าง DO, Line
  SubmitPO = async () => {
    let currentState = this.state;
    // if (currentState.selectedMOPO === "" || currentState.DO === "") {
    //   Swal.fire({
    //     title: "Error!",
    //     text: "Please select MO or scan DO ",
    //     icon: "error",
    //     timer: 1500,
    //   });
    //   return;
    // }

    let test = await httpClient.post(server.SUBMIT_MATERIAL, currentState);
    this.setState(test.data);

    console.log(currentState);

    switch (test.data.status) {
      case "INVALID_SCAN_DO_PO":
        Swal.fire({
          title: "Error!",
          text: "Invalid Scan QR Code DO/PO",
          icon: "error",
          timer: 1500,
        });
        break;
      case "MACHINE_IS_INCORRECTLY_SCANNED":
        Swal.fire({
          title: "Error!",
          text: "Incorrectly Scan Line",
          icon: "error",
          timer: 1500,
        });
        break;
      case "NO_MACHINE_OR_MAT_IS_SELECTED":
        Swal.fire({
          title: "Error!",
          text: "No Machine/Material Selected",
          icon: "error",
          timer: 1500,
        });
        break;
      case "Qrcode is invalid":
        Swal.fire({
          title: "Error!",
          text: "QR Code invalid",
          icon: "error",
          timer: 1500,
        });
        break;
      case "CannotFindMachine":
        Swal.fire({
          title: "Error!",
          text: "Machine not found",
          icon: "error",
          timer: 1500,
        });
        break;

      default:
        Swal.fire({
          title: "Success!",
          text: "Save Success",
          icon: "success",
          timer: 1500,
        });
        break;
    }

    this.setState(test.data);
    setTimeout(function () {
      window.location.reload();
    }, 2000);
  };

  RemoveData = async () => {
    let removedata = await httpClient.get(server.REMOVE_MATERIAL);
    console.log(removedata);
    this.setState(removedata.data);
    this.setState({ status: "" });
  };

  ShowAvailableChild = async () => {
    let test = await httpClient.post(server.GET_SCAN_CHILDPART, {
      MONo: this.state.MOValue,
    });
    switch (test.data) {
      case "Error":
        Swal.fire({
          title: "Error!",
          text: "MO not found",
          icon: "error",
          timer: 1500,
        });
        break;

      default:
        let tableHTML = `<table style="width: 100%; border-collapse: collapse;">
                            <thead>
                                <tr>
                                    <th style="border: 1px solid #ddd; padding: 8px;">#</th>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Child Part Number</th>
                                    <th style="border: 1px solid #ddd; padding: 8px;">Child Part Name</th>
                                </tr>
                            </thead>
                            <tbody>`;

        test.data.forEach((item, index) => {
          tableHTML += `<tr>
                            <td style="border: 1px solid #ddd; padding: 8px;">${
                              index + 1
                            }</td>
                            <td style="border: 1px solid #ddd; padding: 8px;">${
                              item.ChildPartNo
                            }</td>
                            <td style="border: 1px solid #ddd; padding: 8px;">${
                              item.ChildPartName
                            }</td>
                          </tr>`;
        });

        tableHTML += `</tbody></table>`;

        Swal.fire({
          title: "Meterial Require",
          html: tableHTML,
          icon: "info",
          showConfirmButton: true,
        });
        break;
    }
  };

  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header"></div>
            <section className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-header" style={{ fontSize: "30px" }}>
                        Shopper
                      </div>

                      <div className="card-body">
                        <div className="row">
                          <div className="col-6">
                            <div>
                              <label
                                style={{
                                  display: "block",
                                  marginTop: "10px",
                                  marginLeft: "20px",
                                  marginRight: "20px",
                                }}
                              >
                                {" "}
                                Scan MO
                                <input
                                  type="text"
                                  value={this.state.scannedMO}
                                  onChange={this.handleChangeScannedMO}
                                  onKeyPress={(e) => this.scanMOSplit(e)}
                                  className="form-control"
                                  placeholder="input MO"
                                />
                              </label>
                            </div>
                            <br></br>
                          </div>
                          <div
                            className="col-6"
                            style={{ textAlign: "center", marginTop: "2rem" }}
                          >
                            <button
                              className="col-6 btn btn-info"
                              onClick={this.ShowAvailableChild}
                            >
                              Show List
                            </button>
                          </div>

                          <div className="col-6">
                            <div>
                              <label
                                style={{
                                  display: "block",
                                  marginLeft: "20px",
                                  marginRight: "20px",
                                }}
                              >
                                {" "}
                                Select MO
                                <select
                                  className="form-control"
                                  onChange={(e) => this.handleChangeSelectMO(e)}
                                >
                                  <option value="">Select MO</option>
                                  {this.state.savedMOValues.map((eachMO) =>
                                    this.ShowDropDown(eachMO)
                                  )}
                                </select>
                              </label>
                            </div>
                          </div>
                          <div className="col-6">
                            <div>
                              <form onSubmit={this.handleSubmit}>
                                <label
                                  style={{
                                    display: "block",
                                    marginLeft: "20px",
                                    marginRight: "20px",
                                  }}
                                >
                                  Scan Material:
                                  <input
                                    type="text"
                                    value={this.state.scanText}
                                    onChange={this.handleChangeMat}
                                    className="form-control"
                                    placeholder="input material"
                                  ></input>
                                </label>

                                <input
                                  type="submit"
                                  value="Submit"
                                  style={{ visibility: "hidden" }}
                                ></input>
                              </form>
                              {this.state.storedValidMat.map((row) =>
                                this.TestFunction(row)
                              )}
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "flex-end",
                                  marginBottom: "10px",
                                  marginRight: "10px",
                                }}
                              >
                                <button
                                  onClick={this.SubmitAll}
                                  className="btn btn-success"
                                  style={{ marginRight: "10px" }}
                                >
                                  Submit All
                                </button>
                                <button
                                  onClick={this.RemoveData}
                                  className="btn btn-warning"
                                  style={{ marginRight: "10px" }}
                                >
                                  {" "}
                                  Remove{" "}
                                </button>
                                <button
                                  onClick={this.CancelData}
                                  className="btn btn-danger"
                                  style={{ marginRight: "10px" }}
                                >
                                  Cancel
                                </button>
                              </div>
                            </div>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              justifyContent: "flex-end",
                            }}
                          ></div>
                        </div>
                      </div>
                    </div>

                    {/* Card 2 */}
                    <div className="card">
                      <div className="card-body">
                        <div>
                          <label
                            style={{
                              display: "block",
                              marginTop: "10px",
                              marginLeft: "20px",
                              marginRight: "20px",
                            }}
                          >
                            {" "}
                            Select MO Choose Material for use BOM
                            <select
                              className="form-control"
                              onChange={(e) => this.handleChangeSelectMOPO(e)}
                            >
                              <option>Select MO</option>
                              {this.state.availableReceiveMO.map((eachMOPO) =>
                                this.ShowDropDownReceive(eachMOPO)
                              )}
                            </select>
                          </label>
                        </div>
                        <div
                          className="row"
                          style={{
                            marginTop: "10px",
                            marginLeft: "20px",
                            marginRight: "20px",
                          }}
                        >
                          <div className="col-3">
                            <div>
                              <label>
                                Scan DO
                                <input
                                  type="text"
                                  value={this.state.DO}
                                  onChange={this.handleChangeDo}
                                  className="form-control"
                                  placeholder="input DO"
                                  onKeyDown={(e) => this.SubmitDO(e)}
                                />
                              </label>
                            </div>
                          </div>

                          <div className="col-3">
                            <div>
                              <form
                                onSubmit={this.handleSubmitPO}
                                style={{ display: "inline-block" }}
                              >
                                {/* ScanLine */}
                              </form>
                            </div>
                            {this.state.storedMachineGuide.map((row) =>
                              this.ShowMCTable(row)
                            )}
                          </div>
                        </div>

                        <div
                          style={{
                            display: "flex",
                            justifyContent: "flex-end",
                            marginBottom: "10px",
                            marginRight: "10px",
                          }}
                        >
                          <button
                            onClick={this.SubmitPO}
                            className="btn btn-success"
                            style={{ marginRight: "10px" }}
                          >
                            {" "}
                            Submit All
                          </button>
                          <button
                            onClick={this.CancelDataPO}
                            className="btn btn-danger"
                          >
                            {" "}
                            Cancel{" "}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}
