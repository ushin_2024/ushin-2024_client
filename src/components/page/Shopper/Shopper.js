import React, { Component } from "react";
import { Link } from "react-router-dom";
import { OK, server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import Swal from "sweetalert2";


export default class Shopper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DO: "",
      EMP: "",
      MC: "",
      scanMat: "",
      row: 0,
      storedMachine: [],
      storedMat: [],
      status: "",
      previousScannedMat: "",

    };
    this.handleChangeDo = this.handleChangeDo.bind(this);
    this.handleChangeEMP = this.handleChangeEMP.bind(this);
    this.handleChangeMC = this.handleChangeMC.bind(this);
    this.handleChangeScanMat = this.handleChangeScanMat.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount = async () => {
    let getPoState = await httpClient.get(server.GET_POSTATE);
    this.setState(getPoState.data);
    this.setState({ scanMat: "" });
  };

  handleChangeDo = async (event) => {
    this.setState({ DO: event.target.value });
  };
  handleChangeEMP = async (event) => {
    this.setState({ EMP: event.target.value });
  };
  handleChangeMC = async (event) => {
    this.setState({ MC: event.target.value });
  };

  handleChangeScanMat = async (event) => {
    this.setState({ scanMat: event.target.value });
  };

  handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      const inputs = document.querySelectorAll("input");
      const currentIndex = Array.from(inputs).findIndex(
        (input) => input === document.activeElement
      );
      const nextIndex = currentIndex + 1;
      if (inputs[nextIndex]) {
        inputs[nextIndex].focus();
      }
    }
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    let sendScannedMc = await httpClient.post(server.ADD_MACHINE, this.state);
    this.setState(sendScannedMc.data);
    this.setState({ MC: "" });
    // console.log(sendScannedMc.data)

    // console.log(this.state)
  };



  handleKeyPress = async (event) => {
    if (event.key === "Enter") {
      if (this.state.scanMat === this.state.previousScannedMat) {
        Swal.fire({
          title: "Error!",
          text: "Duplicate input detected",
          icon: "error",
          timer: 1500,
        });
        return; 
      }
  
      let sendScannedMat = await httpClient.post(server.ADD_MATERIAL, this.state);
      this.setState(sendScannedMat.data);
      this.setState({ scanMat: "" });
      this.setState({ previousScannedMat: this.state.scanMat });
    }
  };
  

  ShowScannedList(row) {
    let value = [];
    for (let i = 0; i < this.state.storedMatOfEachPO.length; i++) {
      for (let j = 0; j < this.state.storedMatOfEachPO[i].length; j++) {
        if (this.state.storedMatOfEachPO[i][j]["PORow"] === row.row) {
          value.push(this.state.storedMatOfEachPO[i][j]["scannedMat"]);
        }
      }
    }
    return (
      <>
        {value.map((val) => (
          <>
            <br></br>
            {val}
          </>
        ))}
      </>
    );
  }

  ShowMCTable(row) {
    // console.log(row)
    return (
      <>
        <label>{row}</label>
        <br></br>
      </>
    );
  }

  ShowMatTable(row) {
    return (
      <>
        <label>{row}</label>
        <br></br>
      </>
    );
  }

  checkDuplicateInput = (currentValue, previousValue) => {
    return currentValue === previousValue;
  };

  
  SubmitAll = async () => {
    let submitAll = await httpClient.post(server.SUBMIT_ALLPO,this.state);
    this.setState(submitAll.data)
    switch(submitAll.data.status){
      case "INVALID_SCAN_DO_PO":
        Swal.fire({
          title: "Error!",
          text: "Invalid Scan QR Code DO/PO",
          icon: "error",
          timer: 1500,
        });
        break;
      case "NO_MACHINE_OR_MAT_IS_SELECTED":
        Swal.fire({
          title: "Error!",
          text: "No Machine/Material Selected",
          icon: "error",
          timer: 1500,
        });
        break;
      case "Qrcode is invalid":
        Swal.fire({
          title: "Error!",
          text: "QR Code invalid",
          icon: "error",
          timer: 1500,
        });
        break;
        case "CannotFindMachine":
        Swal.fire({
          title: "Error!",
          text: "Machine not found",
          icon: "error",
          timer: 1500,
        });
        break;
      default:
        Swal.fire({
          title: "Success!",
          text: "Save Success",
          icon: "success",
          timer: 1500,
        });
        break;
    }
    console.log(this.state)
  };

  RemoveData = async () => {
    let removedata = await httpClient.get(server.REMOVE_PO);
    console.log(removedata);
    this.setState(removedata.data);
  };

  ShowTable(row) {
    return (
      <>
        <label>{row["MONo"]}&nbsp;</label>
        {this.ShowScannedListNew(row)}
        {/* <label>{row["Emp"]}&nbsp;</label>
            <label>{row["MC"]}&nbsp;</label>
            <label>{row["PartNo"]}</label> */}
        {/* <input
              type="text"
              //   onChange={(e) => this.handleInputChange(row.row, e)}
            //   onKeyPress={(e) => this.handleKeyPress(row, e)}
            ></input> */}
        {/* {this.ShowScannedList(row)} */}
        <br></br>
      </>
    );
  }
  CancelData = async () => {
    let clearedData = await httpClient.get(server.CANCEL_PO);
    console.log(clearedData);
    this.setState(clearedData.data);
    window.location.reload();
  };


  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
          <div className="content-header"></div>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header" style={{ fontSize: "30px", fontFamily : 'poppins'}}>Shopper</div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-3">
                          <div>
                            <label>
                              Scan DO
                              <input
                                type="text"
                                value={this.state.DO}
                                className="form-control"
                                onChange={this.handleChangeDo}
                                placeholder="input DO"
                                onKeyDown={this.handleKeyDown}
                              />
                            </label>
                          </div>
                        </div>
                        <div className="col-3">
                          <div>
                            <label>
                              Scan Employee
                              <input
                                type="text"
                                className="form-control"
                                value={this.state.EMP}
                                onChange={this.handleChangeEMP}
                                placeholder="input Employee ID "
                                onKeyDown={this.handleKeyDown}
                              />
                            </label>
                          </div>
                        </div>

                        <div className="col-3">
                          <div>
                            <form
                              onSubmit={this.handleSubmit}
                              style={{ display: "inline-block" }}
                            >
                              <label>
                                Scan Line
                                <input
                                  type="text"
                                  className="form-control"
                                  value={this.state.MC}
                                  onChange={this.handleChangeMC}
                                  placeholder="input Line"
                                />
                              </label>
                            </form>
                          </div>
                        </div>
                        <div className="col-3">
                          <div>
                            <label>
                              Scan Material
                              <input
                                type="text"
                                className="form-control"
                                value={this.state.scanMat}
                                onChange={this.handleChangeScanMat}
                                onKeyPress={(e) => this.handleKeyPress(e)}
                                placeholder="input Material"
                              />{" "}
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-body">
                      {this.state.storedMachine.map((row) =>
                        this.ShowMCTable(row)
                      )}
                      {this.state.storedMat.map((row) =>
                        this.ShowMatTable(row)
                      )}

                      <div
                        style={{ display: "flex", justifyContent: "flex-end" }}
                      >
                        <button
                          onClick={this.SubmitAll}
                          className="btn btn-success"
                          style={{ marginRight: "10px" }}
                        >
                          {" "}
                          Submit All
                        </button>
                        <button
                          onClick={this.RemoveData}
                          className="btn btn-warning"
                          style={{ marginRight: "10px" }}
                        >
                          {" "}
                          Remove{" "}
                        </button>
                        <button
                          onClick={this.CancelData}
                          className="btn btn-danger"
                        >
                          {" "}
                          Cancel{" "}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
