import React, { Component } from "react";
import { Link } from "react-router-dom";
import { OK, server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class MaterialRequisition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: null,
    };
  }

  handleDateChange = (date) => {
    this.setState({ selectedDate: date });
    console.log(date);
  };

  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header"></div>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div
                      className="card-header"
                      style={{ fontSize: "30px"}}
                    >
                      Material Requisition
                    </div>
                    <div className="card-body">
                      <div className="row"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
