import React, { Component } from "react";
import { Link } from "react-router-dom";
import { OK, server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import moment from "moment";
import "./DO_Data.css";

export default class DO_data extends Component {
  constructor(props) {
    super(props);

    this.state = {
      getDOList: [],
      searchTerm: "", // เพิ่ม state สำหรับเก็บค่าค้นหา
    };
  }

  componentDidMount = async () => {
    await this.getDO();
  };

  getDO = async () => {
    let DOdata = await httpClient.get(server.GET_DO);
    this.setState({ getDOList: DOdata.data });
  };

  handleSearchChange = (e) => {
    this.setState({ searchTerm: e.target.value });
  };

  renderTable() {
    const { getDOList, searchTerm } = this.state;

    // กรองข้อมูลตามค่าค้นหา
    const filteredDOList = getDOList.filter((item) => {
      const DueDate = moment(item.DueDate).format("DD-MMM-YYYY");
      const DeliveryDueDate = moment(item.DeliveryDueDate).format(
        "DD-MMM-YYYY"
      );
      return (
        item.DONo.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.Supp.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.SuppName.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.PartName.toLowerCase().includes(searchTerm.toLowerCase()) ||
        DueDate.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.TotalQty.toString()
          .toLowerCase()
          .includes(searchTerm.toLowerCase()) ||
        DeliveryDueDate.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.CustomersPN.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item["PartList.PartNo"]
          .toLowerCase()
          .includes(searchTerm.toLowerCase()) ||
        item["MasterDOStatus.DOStatus"]
          .toLowerCase()
          .includes(searchTerm.toLowerCase())
      );
    });

    if (filteredDOList != null) {
      return filteredDOList.map((item) => (
        <tr key={item.id}>
          <td>{item.DOId}</td>
          <td>
            <Link
              to=""
              onClick={(e) => {
                e.preventDefault();
              }}
            >
              {item.DONo}
            </Link>
          </td>
          <td>{item.Supp}</td>
          <td>{item.SuppName}</td>
          <td>{item.CMSP}</td>
          <td>{item.Prcs}</td>
          <td>{item.PartName}</td>
          <td>{moment(item.DueDate).format("DD-MMM-YYYY")}</td>
          <td>{item.TotalQty}</td>
          <td>{moment(item.DeliveryDueDate).format("DD-MMM-YYYY")}</td>
          <td>{item.CustomersPN}</td>
          <td>{item["PartList.PartNo"]}</td>
          <td
            style={{
              backgroundColor:
                item["MasterDOStatus.DOStatus"] === "Finish"
                  ? "#36EB62"
                  : item["MasterDOStatus.DOStatus"] === "Reject"
                  ? "#D93644"
                  : "transparent",
            }}
          >
            {item["MasterDOStatus.DOStatus"]}
          </td>
        </tr>
      ));
    }
  }

  render() {
    return (
      <>
        <div className="wrapper" style={{ overflowX: "hidden" }}>
          <div className="content-wrapper">
            <div className="content-header"></div>
            <section className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card card-primary card-outline">
                      <div
                        className="card-header row"
                        style={{ fontSize: "30px" }}
                      >
                        Delivery Order
                        <div
                          className="col"
                          style={{ textAlign: "end", marginLeft: "50rem" }}
                        >
                          <input
                            className="form-control mr-sm-2"
                            type="search"
                            placeholder="Search"
                            value={this.state.searchTerm} // เพิ่มการ binding กับ state
                            onChange={this.handleSearchChange} // เพิ่มฟังก์ชัน handleSearchChange
                            style={{ fontSize: "20px" }}
                            aria-label="Search"
                          />
                        </div>
                      </div>
                      <div className="card-body">
                        <table className="table table-bordered">
                          <thead
                            style={{ fontSize: "12px", fontWeight: "normal" }}
                          >
                            <tr>
                              <th>#</th>
                              <th>DONo</th>
                              <th>Supp</th>
                              <th>SuppName</th>
                              <th>CMSP</th>
                              <th>Prcs</th>
                              <th>PartName</th>
                              <th>Production Due Date</th>
                              <th>TotalQty</th>
                              <th>Delivery Due Date</th>
                              <th>CustomersPN</th>
                              <th>PartNo</th>
                              <th>DOStatus</th>
                            </tr>
                          </thead>
                          <tbody style={{ fontSize: "12px" }}>
                            {this.renderTable()}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}
