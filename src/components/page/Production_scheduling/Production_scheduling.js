import React, { Component } from "react";
import { httpClient } from "../../../utils/HttpClient";
import { server } from "../../../constance/constance";
import moment from "moment";
import Swal from "sweetalert2";

export default class Production_scheduling extends Component {
  constructor(props) {
    super(props);

    this.state = {
      MO_data: [],
      Date: moment().add(-0, "days").format("YYYY-MM-DD"),
      time: "",
      New_Date: "",
      New_Time: "",
      New_MONo: "",
      MOId: "",
      MONo: "",
      DueDate: "",
      createdAt: "",
      updatedAt: "",
      CustomersPN: "",
    };
  }

  async componentDidMount() {
    await this.getMOPrint();
  }
  getMOPrint = async () => {
    let data = await httpClient.get(server.MO_print);
    await this.setState({ MO_data: data.data });
  };
  submit = async () => {  
    if (this.state.New_Date == [] &&  this.state.New_Time == []) {
      await Swal.fire ({ 
        title: "<b> Please comfirm </b>",
        icon: "warning",
        text: "ไม่มีข้อมูลวันที่และเวลา ข้อมูลจะไม่บันทึก"
      }) 
      return
    }
    if (this.state.New_Date == [] ||  this.state.New_Time == []) {
      await Swal.fire ({ 
        title: "<b> Please comfirm </b>",
        icon: "warning",
        text: "ใส่ข้อมูลไม่ครบถ้วน ข้อมูลจะไม่บันทึก"
      }) 
      return
    }

    await Swal.fire({
      title: "<b> Please comfirm </b>",
      text:
        "MO No." +
        this.state.MONo +
        "  Date " +
        this.state.New_Date +
        "  Time:" +
        this.state.New_Time,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, Save!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.UpdateData();
        Swal.fire({
          title: "Success!",
          text: "OK.",
          icon: "success",
          timer: 2000,
        });
        // setTimeout(function () {
        //   window.location.reload();
        // }, 3500);
      }
    });
  };
  UpdateData = async () => {
    let update = await httpClient.post(server.insert_MoDataPrint, {
      MOId: this.state.MOId,
      MONo: this.state.MONo,
      DONo: this.state.DONo,
      DueDate: this.state.DueDate,
      Date: this.state.New_Date,
      Time: this.state.New_Time,
      msg: this.state.msg,
      createdAt: this.state.createdAt,
      updatedAt: this.state.updatedAt,
      CustomersPN: this.state.CustomersPN,
      PartNoId: this.state.PartNoId,
      DOId: this.state.DOId,
    });
    // console.log(update)
  };
  renderTable() {
    if (this.state.MO_data != null) {
      return this.state.MO_data.map((item) => (
        <tr>
          <td>{item.MOId}</td>
          <td>{item.MONo}</td>
          <td>{item.DONo}</td>
          <td>{moment(item.DueDate).format("DD-MMM-YYYY")}</td>
          <td>{item.CustomersPN}</td>
          <td>
            <input
              class="form-control"
              type="date"
              id="id_daydate"
              name="name_daydate"
              // value={this.state.New_Date}
              onChange={async (e) => {
                await this.setState({
                  New_Date: moment(e.target.value).format("YYYY-MM-DD"),
                });
              }}
            />
          </td>
          <td>
            <input
              class="form-control"
              type="time"
              // id="id_daydate"
              name="name_daydate"
              // value={this.state.New_Time}
              onChange={async (e) => {
                await this.setState({
                  New_Time: e.target.value,
                });
              }}
            />
          </td>
          <td>
            <button
              className="btn btn-success"
              type="submit"
              onClick={async (e) => {
                const yourDate = new Date();
                await this.setState({
                  New_Date: this.state.New_Date,
                  New_Time: this.state.New_Time,
                  createdAt: moment(yourDate).format("YYYY-MM-DD HH:mm:ss.000"),
                  updatedAt: moment(yourDate).format("YYYY-MM-DD HH:mm:ss.000"),
                  MOId: item.MOId,
                  MONo: item.MONo,
                  DONo: item.DONo,
                  DueDate: moment(item.DueDate).format("YYYY-MM-DD"),
                  msg: 0,
                  PartNoId: item.PartNoId,
                  CustomersPN: item.CustomersPN,
                  DOId: item.DOId,
                });
                // console.log(this.state)
                await this.submit();
                console.log(this.state);
              }}
            >
              {" "}
              Record
            </button>{" "}
          </td>
        </tr>
      ));
    }
  }
  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1>Production Scheduling</h1>
                  </div>
                </div>
                <div
                  className="container-fluid"
                  style={{ fontSize: "15px"}}
                >
                  {/* Tab card  */}
                  <br />
                  <div className="col-12">
                    <div className="card card-primary card-outline card-outline-tabs">
                      <div className="card-header p-0 border-bottom-0">
                        <ul
                          className="nav nav-tabs"
                          id="custom-tabs-four-tab"
                          role="tablist"
                        >
                          <li className="nav-item">
                            <a
                              className="nav-link active"
                              id="custom-tabs-four-home-tab"
                              data-toggle="pill"
                              href="#custom-tabs-four-home"
                              role="tab"
                              aria-controls="custom-tabs-four-home"
                              aria-selected="true"
                              onClick={(e) => {
                                this.setState({ edit_display: "none" });
                              }}
                            >
                              Date and Time
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="card-body">
                        <div
                          className="tab-content"
                          id="custom-tabs-four-tabContent"
                        >
                          <div
                            className="tab-pane fade show active"
                            id="custom-tabs-four-home"
                            role="tabpanel"
                            aria-labelledby="custom-tabs-four-home-tab"
                          >
                            <div className="card">
                              <div className="card-header">
                                {" "}
                                Please fill Date and Time{" "}
                              </div>
                              <div className="card-body table-responsive p-0">
                                <table className="table table-head-fixed text-nowrap">
                                  <thead>
                                    <tr style={{ padding: 0 }}>
                                      <th>MOid</th>
                                      <th>MONo</th>
                                      <th>DONo</th>
                                      <th>Due Date</th>
                                      <th>Part No</th>
                                      <th>Date</th>
                                      <th>Time</th>
                                      {/* <th>Action</th> */}
                                    </tr>
                                  </thead>
                                  <tbody>{this.renderTable()}</tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
