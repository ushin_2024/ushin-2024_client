import React, { Component } from "react";
import { Link } from "react-router-dom";
import { httpClient } from "../../../utils/HttpClient";
import { OK, server } from "../../../constance/constance";
import moment from "moment";
export default class ChildPartList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ChildPart: [],
    };
  }
  componentDidMount = async () => {
    this.getChildPart();
  };

    getChildPart = async () => {
        let data = await httpClient.get(server.GET_CHILD_PART) 
        console.log(data.data)
        await this.setState({ ChildPart: data.data})
    }
    renderTable() {
        if (this.state.ChildPart != null) {
            return this.state.ChildPart.map((item) =>
                <tr key={item.ChildPartListId}>
                    <td>{item.ChildPartNo}</td>
                    <td>{item.ChildPartName}</td>
                    <td>{item.ProcessName}</td>
                    <td>{item.UsagePerBox}</td>
                    <td>{item.MaterialYieldPercent}</td>
                </tr>
            )
        }
    }
    render() {
        return (
            <div>
                <div className="wrapper">
                    <div className="content-wrapper">
                        <div className="content-header">
                            <div className="container-fluid">
                                <div className="row mb-2">
                                    <div className="col-sm-6">
                                        <h1>Child Part List</h1>
                                    </div>
                                    <div className="col-sm-6">
                                        <ol className="breadcrumb float-sm-right">
                                            <li className="breadcrumb-item"><Link to="/home">Home</Link></li>
                                            <li className="breadcrumb-item active">Child Part List</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>

            <div className="container-fluid">
              <div className="row">
                <div className="col-12">
                  <div className="card card-success card-outline">
                    <div className="card-header">
                      <h3 className="card-title">Part List Master</h3>
                    </div>
                    <div className="card-body">
                      <table
                        className="table table-bordered"
                        style={{ fontSize: "12px", fontWeight: "normal" }}
                      >
                        <thead>
                          <tr>
                            <th>ChildPartNo</th>
                            <th>ChildPartName</th>
                            <th>ProcessName</th>
                            <th>UsagePerBox</th>
                            <th>MaterialYieldPercent</th>
                          </tr>
                        </thead>
                        <tbody>{this.renderTable()}</tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
