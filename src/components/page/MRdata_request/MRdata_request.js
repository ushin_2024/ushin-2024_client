import React, { Component } from "react";
import { Link } from "react-router-dom";
import { httpClient } from "../../../utils/HttpClient";
import { server } from "../../../constance/constance";
import moment from "moment";
import "./MRdata_request.css";
import Swal from "sweetalert2";
import ReactToPrint from "react-to-print";
import MRdata_print from "./MRdata_print";
import MRdata_Reprint from "./MRdata_Reprint";

class MRdata_request extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MO_data: [],
      MO_data_first: [],
      Date: moment().add(-0, "days").format("YYYY-MM-DD"),
      time: "",
      New_Date: "",
      New_Time: "",
      New_MONo: "",
      MOId: "",
      MONo: "",
      DueDate: "",
      Date: "",
      Time: "",
      msg: "",
      msg_update: "",
      createdAt: "",
      updatedAt: "",
      CustomersPN: "",
      printData: [],
      scannedCustomersPN: "",
      scannedMO: "",
      DONo: "",
      MO_Print_Time: " ",
      MO_Print_Date: " ",
      TotalQty: "",
      selectChildPartPrint1: [],
      selectChildPart_RePrint: [],
      table_display: "flex",
      edit_display: "none",
      // state insert log
      ChildPartListId: "",
      ChildPartName: "",
      ChildPartNo: "",
      Package: "",
      RemainingStock: "",
      Short: "",
      TotalQty: "",
      TotalQtyYield: "",
      Usage: "",
      UsagePerBox: "",
    };
  }
  async componentDidMount() {
    await this.getMOPrint();
    console.log(this.state);
  }
  submit = async () => {
    await Swal.fire({
      title: "<b> Please comfirm </b>",
      text:
        "MO No." +
        this.state.MONo +
        "  Date " +
        this.state.New_Date +
        "  Time:" +
        this.state.New_Time,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "ใช่, บันทึก!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.UpdateData();
        Swal.fire({
          title: "Success!",
          text: "OK.",
          icon: "success",
          timer: 2000,
        });
        setTimeout(function () {
          window.location.reload();
        }, 3500);
      }
    });
  };
  UpdateData = async () => {
    let update = await httpClient.post(server.insert_MoDataPrint, {
      MOId: this.state.MOId,
      MONo: this.state.MONo,
      DONo: this.state.DONo,
      DueDate: this.state.DueDate,
      Date: this.state.New_Date,
      Time: this.state.New_Time,
      msg: this.state.msg,
      createdAt: this.state.createdAt,
      updatedAt: this.state.updatedAt,
      CustomersPN: this.state.CustomersPN,
      PartNoId: this.state.PartNoId,
      DOId: this.state.DOId,
    });
    // console.log(update)
  };
  getMOPrint = async () => {
    let data = await httpClient.get(server.MO_print);
    await this.setState({ MO_data: data.data });

    let getMoDataPrint = await httpClient.get(server.getMoDataPrint);
    await this.setState({ MO_data_first: getMoDataPrint.data });

    let getMoDataRePrint = await httpClient.get(server.getMoDataRePrint);
    await this.setState({ MO_data_RePrint: getMoDataRePrint.data });
  };
  renderTable() {
    if (this.state.MO_data != null) {
      return this.state.MO_data.map((item) => (
        <tr>
          <td>{item.MOId}</td>
          <td>{item.MONo}</td>
          <td>{item.DONo}</td>
          <td>{moment(item.DueDate).format("DD-MMM-YYYY")}</td>
          <td>{item.CustomersPN}</td>
          <td>
            <input
              class="form-control"
              type="date"
              id="id_daydate"
              name="name_daydate"
              // value={this.state.New_Date}
              onChange={async (e) => {
                await this.setState({
                  New_Date: moment(e.target.value).format("YYYY-MM-DD"),
                });
              }}
            />
          </td>
          <td>
            <input
              class="form-control"
              type="time"
              // id="id_daydate"
              name="name_daydate"
              // value={this.state.New_Time}
              onChange={async (e) => {
                await this.setState({
                  New_Time: e.target.value,
                });
              }}
            />
          </td>
          <td>
            <button
              className="btn btn-success"
              type="submit"
              onClick={async (e) => {
                const yourDate = new Date();
                await this.setState({
                  New_Date: this.state.New_Date,
                  New_Time: this.state.New_Time,
                  createdAt: moment(yourDate).format("YYYY-MM-DD HH:mm:ss.000"),
                  updatedAt: moment(yourDate).format("YYYY-MM-DD HH:mm:ss.000"),
                  MOId: item.MOId,
                  MONo: item.MONo,
                  DONo: item.DONo,
                  DueDate: moment(item.DueDate).format("YYYY-MM-DD"),
                  msg: 0,
                  PartNoId: item.PartNoId,
                  CustomersPN: item.CustomersPN,
                  DOId: item.DOId,
                });
                // console.log(this.state)
                await this.submit();
                console.log(this.state);
              }}
            >
              {" "}
              บันทึก
            </button>{" "}
          </td>
        </tr>
      ));
    }
  }
  render_TablePrint() {
    if (this.state.MO_data_first != null) {
      const yourDate = new Date();
      return this.state.MO_data_first.map((item) => (
        <tr>
          <td>{item.MOId}</td>
          <td>{item.MONo}</td>
          <td>{item.DONo}</td>
          <td>{moment(item.MO_Print_Date).format("DD-MMM-YYYY")}</td>
          {/* <td>{moment(item.MO_Print_Time).format("HH:mm:ss.000")}</td> */}
          <td>{item.MO_Print_Time}</td>
          <td>{item.CustomersPN}</td>
          <td>{item.TotalQty}</td>
          <td>
            <button
              className="btn btn-primary"
              type="submit"
              onClick={async (e) => {
                e.preventDefault();

                await this.setState({
                  edit_display: "flex",
                  table_display: "none",
                  scannedMO: item.MONo,
                  scannedCustomersPN: item.CustomersPN,
                  DONo: item.DONo,
                  MO_Print_Time: item.MO_Print_Time,
                  MO_Print_Date: item.MO_Print_Date,
                  TotalQty: item.TotalQty,
                });
                await this.selectState();
              }}
            >
              Create MR{" "}
            </button>{" "}
          </td>
        </tr>
      ));
    }
  }
  render_TableRePrint() {
    // console.log(this.state);
    if (this.state.MO_data_RePrint != null) {
      const yourDate = new Date();
      return this.state.MO_data_RePrint.map((item) => (
        <tr>
          <td>{item.MOId}</td>
          <td>{item.MONo}</td>
          <td>{item.DONo}</td>
          <td>{moment(item.MO_Print_Date).format("DD-MMM-YYYY")}</td>
          {/* <td>{moment(item.MO_Print_Time).format("HH:mm:ss.000")}</td> */}
          <td>{item.MO_Print_Time}</td>
          <td>{item.CustomersPN}</td>
          <td>{item.TotalQty}</td>
          <td>
            <button
              className="btn btn-primary"
              type="submit"
              onClick={async (e) => {
                e.preventDefault();

                await this.setState({
                  edit_display: "flex",
                  table_display: "none",
                  scannedMO: item.MONo,
                  scannedCustomersPN: item.CustomersPN,
                  DONo: item.DONo,
                  MO_Print_Time: item.MO_Print_Time,
                  MO_Print_Date: item.MO_Print_Date,
                  TotalQty: item.TotalQty,
                  // updatedAt : item.updatedAt,
                });
                await this.selectState_Reprint();
              }}
            >
              เลือก{" "}
            </button>{" "}
          </td>
        </tr>
      ));
    }
  }
  // เลือก data for print
  selectState = async () => {
    let result_MR = await httpClient.post(server.getChildPartPrint, {
      scannedMO: this.state.scannedMO,
      scannedCustomersPN: this.state.scannedCustomersPN,
      DONo: this.state.DONo,

    });
    var test = result_MR.data;
    this.setState({ selectChildPartPrint1: test });
  };
  // เลือก data for Reprint
  selectState_Reprint = async () => {
    let result_MR1 = await httpClient.post(server.selectState_Reprint, {
      scannedMO: this.state.scannedMO,
      scannedCustomersPN: this.state.scannedCustomersPN,
      DONo: this.state.DONo
    });
    var reprint_data = result_MR1.data;
    this.setState({ selectChildPart_RePrint: reprint_data });
    console.log(reprint_data);
  };

  renderMRData() {
    let qrTable = this.state.selectChildPartPrint1;
    console.log(this.state.selectChildPartPrint1);
    if (qrTable.length > 0) {
      return (
        <div className="card card-info">
          <div className="card-header" style={{ fontSize: "18px" }}>
            <p>Material Requisition {this.TablePrintButton()} </p>
          </div>
          <div className="content">
            <MRdata_print
              printData={qrTable}
              scannedMO={this.state.scannedMO}
              scannedCustomersPN={this.state.scannedCustomersPN}
              DONo={this.state.DONo}
              TotalQty={this.state.TotalQty}
              MO_Print_Date={this.state.MO_Print_Date}
              MO_Print_Time={this.state.MO_Print_Time}
              ref={(el) => (this.componentRef = el)}
            />
          </div>
        </div>
      );
    }
  }

  renderMRData_Reprint() {
    let qrTable_Re = this.state.selectChildPart_RePrint;
    console.log(this.state.selectChildPart_RePrint);
    if (qrTable_Re.length > 0) {
      return (
        <div className="card card-info">
          <div className="card-header" style={{ fontSize: "18px" }}>
            <p>Material Requisition {this.TablePrintButton_Reprint()} </p>
          </div>
          <div className="content">
            <MRdata_Reprint
              printDataRe={qrTable_Re}
              scannedMO={this.state.scannedMO}
              scannedCustomersPN={this.state.scannedCustomersPN}
              DONo={this.state.DONo}
              TotalQty={this.state.TotalQty}
              MO_Print_Date={this.state.MO_Print_Date}
              MO_Print_Time={this.state.MO_Print_Time}
              ref={(el) => (this.componentRef = el)}
            />
          </div>
        </div>
      );
    }
  }
  // update status ของ msg
  updateMsg = async () => {
    const yourDate = new Date();
    let msg_update = await httpClient.put(server.msg_update, {
      scannedMO: this.state.scannedMO,
      scannedCustomersPN: this.state.scannedCustomersPN,
      DONo: this.state.DONo,
      msg_new: this.state.msg_update,
      updatedAt: moment(yourDate).format("YYYY-MM-DD HH:mm:ss.000"),
    });
    console.log(msg_update);
  };
  insert_MRLog = async () => {
    console.log(this.state.selectChildPartPrint1);
    console.log(this.state);
    for (let i = 0; i < this.state.selectChildPartPrint1.length; i++) {
      const data1 = {
        MO_No: this.state.scannedMO,
        DONo: this.state.DONo,
        ChildPartListId: this.state.selectChildPartPrint1[i].ChildPartListId,
        ChildPartName: this.state.selectChildPartPrint1[i].ChildPartName,
        ChildPartNo: this.state.selectChildPartPrint1[i].ChildPartNo,
        Package: this.state.selectChildPartPrint1[i].Package,
        RemainingStock: this.state.selectChildPartPrint1[i].RemainingStock,
        Short: this.state.selectChildPartPrint1[i].Short,
        TotalQty: this.state.selectChildPartPrint1[i].TotalQty,
        TotalQtyYield: this.state.selectChildPartPrint1[i].TotalQtyYield,
        Usage: this.state.selectChildPartPrint1[i].Usage,
        UsagePerBox: this.state.selectChildPartPrint1[i].UsagePerBox,
      };
      let insert_MRLog = await httpClient.post(server.insertMoDataLog, data1);
      console.log(insert_MRLog);
    }
  };

  TablePrintButton = () => {
    return (
      <>
        <button
          className="btn btn-secondary float-right"
          style={{ fontSize: "18px" }}
          onClick={async () => {
            this.setState({
              edit_display: "none",
              scannedMO: "",
              scannedCustomersPN: "",
            });
            await window.location.reload();
          }}
        >
          <b>Clear </b>
        </button>
        <button
          className="btn btn-primary float-right"
          onClick={async () => {
            await this.setState({
              msg_update: 1,
            });
            await this.updateMsg();
            await this.insert_MRLog();
          }}
        >
          <ReactToPrint
            trigger={() => <a href="#">Print MR</a>}
            content={() => this.componentRef}
          />
        </button>
      </>
    );
  };

  TablePrintButton_Reprint = () => {
    return (
      <>
        <button
          className="btn btn-secondary float-right"
          style={{ fontSize: "18px" }}
          onClick={async () => {
            this.setState({
              edit_display: "none",
              scannedMO: "",
              scannedCustomersPN: "",
            });
            await window.location.reload();
          }}
        >
          <b>Clear </b>
        </button>
        <button
          className="btn btn-primary float-right"
          onClick={async () => {
            await this.setState({
              msg_update: 1,
            });
            await this.updateMsg();
            // await this.insert_MRLog();
          }}
        >
          <ReactToPrint
            trigger={() => <a href="#">Print MR</a>}
            content={() => this.componentRef}
          />
        </button>
      </>
    );
  };

  render() {
    return (
      <div className="wrapper">
        <div className="content-wrapper">
          <div className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1>Material Requisition</h1>
                </div>
              </div>
              <div
                className="container-fluid"
                style={{ fontSize: "15px" }}
              >
                {/* Tab card  */}
                <br />
                <div className="col-12">
                  <div className="card card-primary card-outline card-outline-tabs">
                    <div className="card-header p-0 border-bottom-0">
                      <ul
                        className="nav nav-tabs"
                        id="custom-tabs-four-tab"
                        role="tablist"
                      >
                        <li className="nav-item">
                          <a
                            className="nav-link active"
                            id="custom-tabs-four-profile-tab"
                            data-toggle="pill"
                            href="#custom-tabs-four-profile"
                            role="tab"
                            aria-controls="custom-tabs-four-profile"
                            aria-selected="false"
                            onClick={(e) => {
                              this.setState({ edit_display: "none" });
                            }}
                          >
                            {" "}
                            Print{" "}
                          </a>
                        </li>
                        <li className="nav-item">
                          <a
                            className="nav-link"
                            id="custom-tabs-four-messages-tab"
                            data-toggle="pill"
                            href="#custom-tabs-four-messages"
                            role="tab"
                            aria-controls="custom-tabs-four-messages"
                            aria-selected="false"
                            onClick={(e) => {
                              this.setState({ edit_display: "none" });
                            }}
                          >
                            Re-Print
                          </a>
                        </li>
                      </ul>
                    </div>
                    <div className="card-body">
                      <div
                        className="tab-content"
                        id="custom-tabs-four-tabContent"
                      >
                        <div
                          className="tab-pane fade show active"
                          id="custom-tabs-four-profile"
                          role="tabpanel"
                          aria-labelledby="custom-tabs-four-profile-tab"
                        >
                          <div className="card">
                            <div className="card-header">
                              {" "}
                              Print Material Requisition{" "}
                            </div>
                            <div className="card-body table-responsive p-0">
                              <table className="table table-bordered">
                                <thead>
                                  <tr>
                                    <th style={{ width: 10 }}>MOid</th>
                                    <th>MoNo</th>
                                    <th>DoNo</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>CustomersPN</th>
                                    <th>ToTalQty</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.render_TablePrint()}
                                  {/* {this.TablePrintButton()} */}
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div
                          className="tab-pane fade"
                          id="custom-tabs-four-messages"
                          role="tabpanel"
                          aria-labelledby="custom-tabs-four-messages-tab"
                        >
                          <div className="card">
                            <table className="table table-bordered">
                              <thead>
                                <tr>
                                  <th style={{ width: 10 }}>MOid</th>
                                  <th>MoNo</th>
                                  <th>DoNo</th>
                                  <th>Date</th>
                                  <th>Time</th>
                                  <th>CustomersPN</th>
                                  <th>ToTalQty</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>{this.render_TableRePrint()}</tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="card card-warning"
                style={{
                  display: this.state.edit_display,
                }}
              >
                <div className="card-body" id="custom-tabs-four-profile">
                  {this.renderMRData()}
                </div>
                <div className="card-body" id="custom-tabs-four-messages">
                  {this.renderMRData_Reprint()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MRdata_request;
