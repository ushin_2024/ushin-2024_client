import React, { Component } from "react";

export default class MRdata_Reprint extends Component {
  printDataRe = this.props.printDataRe;
  customer_PN = this.props.scannedCustomersPN;
  Mo_no = this.props.scannedMO;
  TotalQty = this.props.TotalQty;
  ChildPartPrint = this.props.selectChildPartPrint1;
  // ChildPartNo = this.props.selectChildPartPrint1.ChildPartNo;
  MO_Print_Date = this.props.MO_Print_Date;
  MO_Print_Time = this.props.MO_Print_Time;
  DONo = this.props.DONo

  render() {
    return (
      <div style={{ margin: "20px", padding: "50px" }}>
        <section className="content">
          <div className="container-fluid">
            <h1 style={{ textAlign: "center" }}>
              <b>Material Requisition</b>
            </h1>{" "}
            <br />
            <div className="row" style={{ textAligntext: "center" }}>
            <div className="col-md-3">
                <h5> Customer_PN.: {this.customer_PN} </h5>
              </div>
              <div className="col-md-4">
                <h5> MO Number : {this.Mo_no} </h5>
              </div>
              <div className="col-md-3">
                <h5> DO Number : {this.DONo} </h5>
              </div>
              <div className="col-md-2">
                <h5> TotalQty : {this.TotalQty}</h5>
              </div>
            </div>
            <label> Date : {this.MO_Print_Date}</label>{" "}
            <label> Time : {this.MO_Print_Time}</label>
            <div className="card-body">
              <div className="row">
                <div className="table table">
                  <table>
                    <thead>
                      <tr style={{ border: "2px solid black" }}>
                        <th style={{ border: "1px solid black" }}>
                          {" "}
                          PART_NO.{" "}
                        </th>
                        <th style={{ border: "1px solid black" }}>
                          {" "}
                          PART_NAME{" "}
                        </th>
                        <th style={{ border: "1px solid black" }}>
                          {" "}
                          จำนวนทั้งหมดที่ต้องใช้{" "}
                        </th>
                        <th style={{ border: "1px solid black" }}> Package </th>
                        <th style={{ border: "1px solid black" }}>
                          RemainingStock{" "}
                        </th>
                        <th style={{ border: "1px solid black" }}>
                          จำนวนที่ต้องการเพิ่ม{" "}
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.printDataRe.map((printItem, index) => (
                        <tr
                          key={index}
                          style={{
                            border: "2px solid black",
                            margin: "15px 0 30px 0",
                          }}
                        >
                          <td style={{ border: "1px solid black" }}>
                            {printItem.ChildPartNo}
                          </td>
                          <td style={{ border: "1px solid black" }}>
                            {printItem.ChildPartName}
                          </td>

                          <td style={{ border: "1px solid black" }}>
                            {printItem.TotalQty}
                          </td>
                          <td style={{ border: "1px solid black" }}>
                            {printItem.Package}
                          </td>
                          <td style={{ border: "1px solid black" }}>
                            {printItem.MatRemain}
                          </td>
                          <td style={{ border: "1px solid black" }}>
                            {printItem.Short}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
