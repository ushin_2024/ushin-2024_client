import React, { Component } from "react";
import Swal from "sweetalert2";
import { OK, server, APP_TITLE, key, YES } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";

export default class loginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      emp_no: "",
    };
  }
  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      // this.props.login(this.props.history, this.state);
      this.doLogin();
    }
  };
  doLogin = async () => {
    let Login_result = await httpClient.post(server.LOGIN_URL, this.state);
    if (Login_result.data.api_result === OK) {
      Swal.fire({
        icon: "success",
        title: "Welcome to the web-site of",
        text: { APP_TITLE }.APP_TITLE,
        showConfirmButton: false,
        // timer: 100000,
      });

      // console.log(Login_result.data.result);
      localStorage.setItem(key.LOGIN_PASSED, YES);
      localStorage.setItem(key.USER_NAME, Login_result.data.result.EmployeeId);
      localStorage.setItem(key.USER_EMP, Login_result.data.result.EmployeeName);
      // localStorage.setItem(key.TIME_LOGIN, moment());
      // localStorage.setItem(key.USER_LV, Login_result.data.result.levelUser);
      window.location.replace("../home");
    } else {
      Swal.fire({
        icon: "error",
        title: "Log in Fail",
        text: "Network Disconnected",
      });

      return;
    }
  };

  render() {
    return (
      <>
        <div className="login-page">
          <div className="login-box">
            <div className="login-logo">
              <a href="../../index2.html">
                <b>Admin</b>LTE
              </a>
            </div>
            <div className="card">
              <div className="card-body login-card-body">
                <p className="login-box-msg">Sign in to start your session</p>
                <form action="../../index3.html" method="post">
                  <div className="input-group mb-3">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="EmpNo"
                      value={this.state.emp_no}
                      onChange={async (e) => {
                        e.preventDefault();
                        await this.setState({ emp_no: e.target.value });
                      }}
                      onKeyPress={this.handleKeyPress}
                    />
                    <div className="input-group-append">
                      <div className="input-group-text">
                        <span className="fas fa-user" />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-4"></div>
                    <div className="col-4">
                      <button
                        type="submit"
                        className="btn btn-primary btn-block"
                      >
                        Sign In
                      </button>
                    </div>
                  </div>
                </form>

                <p className="mb-0">
                  <a href="register.html" className="text-center">
                    Register a new membership
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
