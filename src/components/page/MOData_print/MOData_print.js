import React, { Component } from "react";
import { Link } from "react-router-dom";
import { httpClient } from "../../../utils/HttpClient";
import { server } from "../../../constance/constance";
import moment from "moment";

import Swal from "sweetalert2";

class MODataPrint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MO_data: [],
      MO_data_first: [],
      Date: moment().add(-0, "days").format("YYYY-MM-DD"),
      time: "",
      New_Date: "",
      New_Time: "",
      New_MONo: "",
      MOId: "",
      MONo: "",
      DueDate: "",
      Date: "",
      Time: "",
      msg: "",
      createdAt: "",
      updatedAt: "",
      CustomersPN: "",

      printData: [],
    };
  }
  componentDidMount = async () => {
    await this.getMOPrint();
  };
  submit = async () => {
    await Swal.fire({
      title: "<b> Please comfirm </b>",
      text: this.state.MONo + this.state.New_Date + this.state.New_Time,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "ใช่, บันทึก!",
    }).then((result) => {
      if (result.isConfirmed) {
        this.UpdateData();
        Swal.fire({
          title: "Success!",
          text: "OK.",
          icon: "success",
        });
      }
    });
  };
  UpdateData = async () => {
    let update = await httpClient.post(server.insert_MoDataPrint, {
      MOId: this.state.MOId,
      MONo: this.state.MONo,
      DueDate: this.state.DueDate,
      Date: this.state.New_Date,
      Time: this.state.New_Time,
      msg: this.state.msg,
      createdAt: this.state.createdAt,
      updatedAt: this.state.updatedAt,
      CustomersPN: this.state.CustomersPN,
      PartNoId: this.state.PartNoId,
      DOId: this.state.DOId,
    });
    console.log(update);
  };
  getMOPrint = async () => {
    let data = await httpClient.get(server.MO_print);
    await this.setState({ MO_data: data.data });
    console.log(data);

    let getMoDataPrint = await httpClient.get(server.getMoDataPrint);
    await this.setState({ MO_data_first: getMoDataPrint.data });
    // console.log(getMoDataPrint)
  };
  renderTable() {
    if (this.state.MO_data != null) {
      return this.state.MO_data.map((item) => (
        <tr>
          <td>{item.MOId}</td>
          <td>{item.MONo}</td>
          <td>{moment(item.DueDate).format("DD-MMM-YYYY")}</td>
          <td>{item.CustomersPN}</td>
          <td>
            <input
              class="form-control"
              type="date"
              id="id_daydate"
              name="name_daydate"
              onChange={async (e) => {
                await this.setState({
                  New_Date: moment(e.target.value).format("YYYY-MM-DD"),
                });
              }}
            />
          </td>
          <td>
            <input
              class="form-control"
              type="time"
              id="id_daydate"
              name="name_daydate"
              onChange={async (e) => {
                await this.setState({
                  New_Time: e.target.value,
                });
              }}
            />
          </td>
          <td>
            <button
              className="btn btn-success"
              type="submit"
              onClick={async (e) => {
                const yourDate = new Date();
                await this.setState({
                  New_Date: moment(yourDate).format("YYYY-MM-DD"),
                  New_Time: moment(yourDate).format("HH:mm:ss.000"),
                  createdAt: moment(yourDate).format("YYYY-MM-DD HH:mm:ss.000"),
                  updatedAt: moment(yourDate).format("YYYY-MM-DD HH:mm:ss.000"),
                  MOId: item.MOId,
                  MONo: item.MONo,
                  DueDate: moment(item.DueDate).format("YYYY-MM-DD"),
                  msg: "0",
                  PartNoId: item.PartNoId,
                  CustomersPN: item.CustomersPN,
                  DOId: item.DOId,
                });
                console.log(this.state);
                await this.submit();
              }}
            >
              {" "}
              บันทึก
            </button>{" "}
          </td>
        </tr>
      ));
    }
  }
  render_TablePrint() {
    console.log(this.state.MO_data_first);
    const yourDate = new Date();
    if (this.state.MO_data_first != null) {
      return this.state.MO_data_first.map((item) => (
        <tr>
          <td>{item.MOId}</td>
          <td>{item.MONo}</td>
          <td>{moment(item.MO_Print_Date).format("DD-MMM-YYYY")}</td>
          <td>{item.MO_Print_Time}</td>
          <td>{item.CustomersPN}</td>
          <td>
            <button className="btn btn-primary" type="submit">
              Print
            </button>{" "}
          </td>
        </tr>
      ));
    }
  }
  render() {
    return (
      <div className="wrapper">
        <div className="content-wrapper">
          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1 style={{fontSize: "35px" }}>
                    {" "}
                    Material Requisition{" "}
                  </h1>
                </div>
                <div className="col-sm-6"></div>
              </div>
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-6">
                    <div className="card">
                      <div
                        className="card-header"
                       
                      >
                        {" "}
                        Material Requisition{" "}
                      </div>
                      Please fill Date and Time
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6">
                            <table className="table table-bordered">
                              <thead>
                                <tr style={{ padding: 0 }}>
                                  <th>MOid</th>
                                  <th>MoNo</th>
                                  <th style={{ width: 10 }}>Due Date</th>
                                  <th>Part No</th>
                                  <th>Date</th>
                                  <th>Time</th>
                                  {/* <th>Action</th> */}
                                </tr>
                              </thead>
                              <tbody style={{ width: "auto" }}>
                                {this.renderTable()}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* new card  */}
                  </div>
                  <div className="col-md-6">
                    <div className="card">
                      <div className="card-header"> Print </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6">
                            <table className="table table-bordered">
                              <thead>
                                <tr>
                                  <th style={{ width: 10 }}>MOid</th>
                                  <th>MoNo</th>
                                  <th>Date</th>
                                  <th>Time</th>
                                  <th>CustomersPN</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>{this.render_TablePrint()}</tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="card">
                      <div className="card-header"> Re Print </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6">
                            <table className="table table-bordered">
                              <thead>
                                <tr>
                                  <th style={{ width: 10 }}>MOid</th>
                                  <th>MoNo</th>
                                  <th>Due Date</th>
                                  <th>Time</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>

                    {/* new card  */}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}

export default MODataPrint;
