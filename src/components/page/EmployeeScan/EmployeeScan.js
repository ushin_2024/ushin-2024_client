import React, { Component } from "react";
import { Link } from "react-router-dom";
import { OK, server } from "../../../constance/constance";
import { httpClient } from "../../../utils/HttpClient";
import Swal from "sweetalert2";

export default class EmployeeScan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scannedEmp: "",
      scannedMC: "",
    };
    this.handleChangeEmp = this.handleChangeEmp.bind(this);
    this.handleChangeMachine = this.handleChangeMachine.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChangeEmp = async (event) => {
    this.setState({ scannedEmp: event.target.value });
  };
  handleChangeMachine = async (event) => {
    this.setState({ scannedMC: event.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();

    let test = await httpClient.post(server.SUBMIT_EMPLOYEE, {
      data: {
        scannedEmp: String(this.state.scannedEmp),
        scannedMC: String(this.state.scannedMC),
      },
    });
    // this.setState(sendData.data);
    console.log(test.data);

    console.log(test);
    switch (test.data) {
      case "MACHINE_NOT_FOUND_IN_DATABASE":
        Swal.fire({
          title: "Error!",
          text: "Machine Not Found",
          icon: "error",
          timer: 1500,
        });
        break;
      case "EMPLOYEE_NOT_FOUND_IN_DATABASE":
        Swal.fire({
          title: "Error!",
          text: "Employee Not Found",
          icon: "error",
          timer: 1500,
        });
        break;
      default:
        Swal.fire({
          title: "Success!",
          text: "Save Success",
          icon: "success",
          timer: 1500,
        });
        break;
    }
    // console.log(this.state.scannedPONo)
    this.setState({ scannedEmp: "" });
    this.setState({ scannedMC: "" });
  };

  handleKeyDown = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      const inputs = document.querySelectorAll("input");
      const currentIndex = Array.from(inputs).findIndex(
        (input) => input === document.activeElement
      );
      const nextIndex = currentIndex + 1;
      if (inputs[nextIndex]) {
        inputs[nextIndex].focus();
      }
    }
  };

  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header"></div>
            <section className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div
                        className="card-header"
                        style={{ fontSize: "30px"}}
                      >
                        Scan Employee
                      </div>
                      <div className="card-body">
                        <div style={{ margin: "7px 0 0 20px" }}>
                          <label style={{}}>
                            Scan Employee ID
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Input Emp ID"
                              value={this.state.scannedEmp}
                              onChange={this.handleChangeEmp}
                              onKeyDown={this.handleKeyDown}
                            ></input>
                          </label>

                          <form
                            onSubmit={this.handleSubmit}
                            style={{
                              display: "inline-block",
                              margin: "0 0 0 20px",
                            }}
                          >
                            <label style={{}}>
                              Scan Line
                              <input
                                type="text"
                                className="form-control"
                                placeholder="Input Line"
                                value={this.state.scannedMC}
                                onChange={this.handleChangeMachine}
                                // onKeyDown={this.handleKeyDown}
                              ></input>
                            </label>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}
