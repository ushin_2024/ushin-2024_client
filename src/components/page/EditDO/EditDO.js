import React, { useState, useEffect } from "react";
import { httpClient } from "../../../utils/HttpClient";
import { server } from "../../../constance/constance";
import Swal from "sweetalert2";

const EditDO = () => {
  const [doData, setDOData] = useState([]);
  const [POData, setPOData] = useState([]);
  const [selectedData, setSelectedData] = useState({
    DONo: "",
    PONo: "",
  });
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchDOData();
  }, []);

  const fetchDOData = async () => {
    try {
      let getDONo = await httpClient.get(server.GET_ALLDO);
      setDOData(getDONo.data);
    } catch (error) {
      setError(error);
    }
  };

  const fetchPOData = async (DO) => {
    try {
      let getPONo = await httpClient.post(server.GET_ALLPO, { data: DO });
      setPOData(getPONo.data);
    } catch (error) {
      setError(error);
    }
  };

  const renderDO = (val) => {
    return <option value={val}>{val}</option>;
  };

  const renderPO = (val) => {
    return <option value={val}>{val}</option>;
  };

  const getPOFromSelectedDO = (event) => {
    const selectDO = event.target.value;
    fetchPOData(selectDO);
    setSelectedData((prev) => ({ ...prev, DONo: selectDO }));
  };

  const SelectPO = (event) => {
    const selectPO = event.target.value;
    setSelectedData((prev) => ({ ...prev, PONo: selectPO }));
  };

  const ClickDelete = async () => {
    try {
      console.log(selectedData);
      let deletePO = await httpClient.post(server.DELETEPO, selectedData);
      console.log(deletePO.data);
      switch (deletePO.data) {
        case "OK":
          Swal.fire({
            title: "Success!",
            text: "Delete PO Success",
            icon: "success",
            timer: 1500,
          });
          break;
        case "TypeError":
          Swal.fire({
            title: "Error!",
            text: "Type Error",
            icon: "error",
            timer: 1500,
          });
          break;
      
        default:
          break;
      }
    } catch (error) {
      setError(error);
    }
  };

  return (
    <>
      <div className="wrapper">
        <div className="content-wrapper">
          <div className="content-header"></div>
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header" style={{ fontSize: "30px" }}>
                      Delete DO/ PO Card
                    </div>

                    <div className="card-body">
                      <div className="row">
                        <div className="col-4 mt-3">
                          <div>
                            <label
                              style={{
                                display: "block",
                                marginLeft: "20px",
                                marginRight: "20px",
                              }}
                            >
                              {" "}
                              Select DO
                              <div>
                                <select
                                  className="form-control"
                                  onChange={getPOFromSelectedDO}
                                >
                                  <option selected="true" disabled="disabled">
                                    Select DO
                                  </option>
                                  {doData.map((e) => renderDO(e))}
                                </select>
                              </div>
                            </label>
                          </div>
                        </div>
                        <div className="col-4 mt-3">
                          <div>
                            <label
                              style={{
                                display: "block",
                                marginLeft: "20px",
                                marginRight: "20px",
                              }}
                            >
                              {" "}
                              Select PO
                              <div>
                                <select
                                  className="form-control"
                                  onChange={SelectPO}
                                >
                                  <option selected="true" disabled="disabled">
                                    Select PO
                                  </option>
                                  {POData.map((e) => renderPO(e))}
                                </select>
                              </div>
                            </label>
                          </div>
                        </div>
                        <div className="col-4 mt-3">
                          <button
                            className="btn btn-danger"
                            style={{ marginTop: "20px" }}
                            onClick={ClickDelete}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                      {error && (
                        <div className="alert alert-danger" role="alert">
                          {error.message}
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </>
  );
};

export default EditDO;
