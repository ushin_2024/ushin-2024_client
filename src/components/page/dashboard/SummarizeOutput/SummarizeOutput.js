import * as moment from "moment";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { httpClient } from "../../../../utils/HttpClient";
import { server } from "../../../../constance/constance";
import Swal from "sweetalert2";
import ReactApexChart from "react-apexcharts";
import { Bar } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import Chart, { defaults, Colors } from "chart.js/auto";

import "./series.css";
defaults.maintainAspectRatio = false;
defaults.responsive = true;
defaults.plugins.title.font.size = 20;
Chart.register(Colors);
Chart.register(ChartDataLabels);

export default class SummarizeOutput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ttlOutput: [],
            date: moment().add(-0, "days").format("YYYY-MM-DD"),

      options: {},
      series: [],
      chart_Label: [],
      chart_show_data: [],
      chart_color: [],
    };
  }
  componentDidMount = () => {
    this.ttlOutput();
  };
  ttlOutput = async () => {
    const data = await httpClient.post(server.getSummarizeOutput, {
      DueDate: this.state.date,
    });
    await this.setState({ ttlOutput: data.data });
    console.log(data.data);
    await this.TableTTL();
    var label_ttl = [];
    var TTl_data = [];
    console.log(this.state.ttlOutput);
    for (var i = 0; i < data.data.length; i++) {
      label_ttl.push(data.data[i].CustomersPN);
      TTl_data.push(data.data[i].totalSum);
    }
    console.log(TTl_data);
    await this.setState({
      chart_Label: label_ttl,
      chart_show_data: TTl_data,
    });
  };
  TableTTL() {
    if (this.state.ttlOutput != null) {
      return this.state.ttlOutput.map((item) => (
        <tr>
          <td>{item.PartName}</td>
          <td>{item.CustomersPN} </td>
          <td>{item.totalSum} </td>
        </tr>
      ));
    }
  }
  render() {
    var ToTalChart = {
      labels: this.state.chart_Label,
      datasets: [
        {
          label: "Total",
          data: this.state.chart_show_data,
          backgroundColor: [
            "rgba(255, 153, 153, 1)",
            "rgba(93, 222, 116,1)",
            "rgba(102, 102, 255, 1)",
            "rgba(255,153,51,1)",
            "rgba(255, 153,255,1)",
            "rgba(247,247,87,1)",
            "rgba(0, 186, 149,1)",
            "rgba(22, 0, 186,1)",
            "rgba(194,157,11,1)",
            "rgba(196, 15,241,1)",
          ],
          borderWidth: 2,
          borderRadius: 5,
        },
      ],
    };
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <section className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1 style={{ fontSize: "30px" }}>Summarize Output</h1>
                  </div>
                  <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                      <li className="breadcrumb-item">
                        <Link to="/DashBoard">Dashboard</Link>
                      </li>
                      <li className="breadcrumb-item active">
                        SummarizeOutput
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </section>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-12">
                  <div className="card card-primary card-outline">
                    <div className="card-header">Summarize Output</div>
                    <div className="card-body">
                      <div className="col-md-12">
                        <div className="row">
                          <div className="col-md-1"> </div>
                          {/* <div className="col-md-2">
                                                        <h5>
                                                            <i class="fas fa-calendar-day">&nbsp;</i>DATE
                                                        </h5>
                                                        <input
                                                            class="form-control is-valid"
                                                            type="date"
                                                            id="id_daydate"
                                                            name="name_daydate"
                                                            value={this.state.date}
                                                            onChange={async (e) => {
                                                                await this.setState({
                                                                    date: moment(e.target.value).format("YYYY-MM-DD"),
                                                                });
                                                            }}
                                                        />

                                                    </div> */}
                          {/* <div className="col-md-1">
                                                        <div class="form-group" >
                                                            <br />
                                                            <button type="submit" class="btn btn-block btn-success"
                                                                onClick={async (e) => {
                                                                    await e.preventDefault();
                                                                    await this.ttlOutput();
                                                                }} style={{ fontSize: '16px' }}
                                                            > <i className="fas fa-search" /> &nbsp; SEARCH </button>
                                                        </div>

                                                    </div> */}
                                                </div>
                                                <div className='col-md-6'>
                                                    <div className="card-body">
                                                        <table className="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>PartName</th>
                                                                    <th>CustomersPN </th>
                                                                    <th>ToTal </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody> {this.TableTTL()}</tbody>
                                                        </table>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div className="col-md-12">
                                            <div className="row">
                                                <div className="col-md-12" >
                                                    <div className="card-body">
                                                        {/*  chart course */}
                                                        <div
                                                            className="chart"
                                                            style={{ height: 300, }}
                                                        >
                                                            <Bar
                                                                data={ToTalChart}
                                                                options={{
                                                                    plugins: {
                                                                        maintainAspectRatio: false,
                                                                        responsive: true,
                                                                        title: {
                                                                            display: true,
                                                                            text: "Total Outputs",
                                                                        },
                                                                        datalabels: {
                                                                            anchor: 'end',
                                                                            align: 'top',
                                                                            font: {
                                                                                weight: 'bold'
                                                                            }
                                                                        },
                                                                        legend: {
                                                                            display: false,
                                                                        },
                                                              
                                                                    },

                                                                }}
                                                            ></Bar>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            {/* end */}

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    }
}
