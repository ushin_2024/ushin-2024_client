import React, { Component } from 'react'
import { getTTFB } from 'web-vitals'
import { httpClient } from '../../../../utils/HttpClient'
import { server } from "../../../../constance/constance";
import { Link } from 'react-router-dom';

export default class StockMat extends Component {
    constructor(props) {
        super(props)

        this.state = {
            matData: [],

        }
    }
    componentDidMount = () => {
        this.getMatInline()
        // this.refreshPage();
    }
    // refreshPage() {
    //     setTimeout(() => {
    //         window.location.reload(false);
    //     }, 5000000);
    //     console.log('page to reload')
    // }
    getMatInline = async () => {
        let remainMatInline = await httpClient.get(server.getMaterialInlineStock)
        this.setState({ matData: remainMatInline.data })
        console.log(remainMatInline.data[0].OutOfStock)
    }
    renderTB() {
        if (this.state.matData != null) {
            return this.state.matData.map((item) =>
                <tr key={item.No}>
                    {/* <td>{item.ChildPartListId}</td> */}
                    <td>{item.ChildPartNo} </td>
                    <td>{item.ChildPartName} </td>
                    <td>{item.QtyLeft}</td>
                    <td style={{ color: item.OutOfStock == "Usable" ? "#35ed38" : "transparent" }}>{item.OutOfStock}</td>
                </tr>
            )
        }
    }

    render() {
        return (
            <>
                <div className="wrapper">
                    <div className='content-wrapper'>
                        <section className="content-header">
                            <div className="container-fluid">
                                <div className="row mb-2">
                                    <div className="col-sm-6">
                                        <h1>Material</h1>
                                    </div>
                                    <div className="col-sm-6">
                                        <ol className="breadcrumb float-sm-right">
                                            <li className="breadcrumb-item"><Link to="/home">Home</Link></li>
                                            <li className="breadcrumb-item active"> Material in line </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <div className='container-fluid' >
                            <div className="card card-info">
                                <div className="card-header">
                                    <h3 className="card-title"> Material In Porduction Line </h3>
                                </div>
                                <form className="form-horizontal">
                                    <div className="card-body">
                                        <div className="card-body">
                                            <table className="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        {/* <th style={{ width: 10 }}>ID</th> */}
                                                        <th>ChildPartNo</th>
                                                        <th>ChildPartName</th>
                                                        <th>QtyLeft</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.renderTB()}
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
