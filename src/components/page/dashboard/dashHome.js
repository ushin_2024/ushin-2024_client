import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  FcSearch,
  FcServices,
  FcComboChart,
  FcInTransit,
  FcDataSheet,
} from "react-icons/fc";

export default class dashHome extends Component {
  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header"></div>
            <section className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-6">
                    <div className="card card-primary card-outline">
                      <div className="card-header">
                        <h3 className="card-title">
                          {" "}
                          <p style={{ fontSize: "30px"}}> Data</p>{" "}
                        </h3>
                      </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6 col-6">
                            <div
                              className="small-box"
                              style={{ backgroundColor: "#C59C70" , margin:"10px 0 0 10px" }}
                            >
                              <div className="inner">
                                <h4> Traceability </h4>
                                <p>Check Data</p>
                              </div>
                              <div className="icon">
                                <FcDataSheet />
                              </div>
                              <Link
                                to="/Traceability"
                                className="small-box-footer"
                              >
                                More info
                                <i className="fas fa-arrow-circle-right" />
                              </Link>
                            </div>
                          </div>
                          <div className="col-md-6 col-6" >
                            <div
                              className="small-box"
                              style={{ backgroundColor: "#E3C281" , margin:"10px 10px 10px 0"}}
                            >
                              <div className="inner">
                                <h4>Delivery Order</h4>
                                <p>Check DO Detail </p>
                              </div>
                              <div className="icon">
                                <FcInTransit />
                              </div>

                              <Link
                                to="/DeliveryData"
                                className="small-box-footer"
                              >
                                More info{" "}
                                <i className="fas fa-arrow-circle-right" />
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <div className="col-6">
                    <div className="card card-info card-outline">
                      <div className="card-header">
                        <h3 className="card-title">
                          {" "}
                          <p style={{ fontSize: "30px"}}> Dashboard </p>
                        </h3>
                      </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-6 col-6">
                            <div
                              className="small-box "
                              style={{ backgroundColor: "#FAA8B6" }}
                            >
                              <div className="inner">
                                <h4>Summarize output</h4>
                                <p>output data by customer P/N</p>
                              </div>
                              <div className="icon">
                                <FcComboChart />
                              </div>
                              <Link
                                to="/SummarizeOutput"
                                className="small-box-footer"
                              >
                                More info{" "}
                                <i className="fas fa-arrow-circle-right" />
                              </Link>
                            </div>
                          </div>
                          <div className="col-md-6 col-6">
                            <div
                              className="small-box "
                              style={{ backgroundColor: "#94a2ed" }}
                            >
                              <div className="inner">
                                <h4>Production Result</h4>
                                <p> FG output each DO </p>
                              </div>
                              <div className="icon">
                                <FcSearch />
                              </div>
                              <Link to="/ProdData" className="small-box-footer">
                                More info{" "}
                                <i className="fas fa-arrow-circle-right" />
                              </Link>
                            </div>
                          </div>
                          <div className="col-md-6 col-6">
                            <div
                              className="small-box"
                              style={{ backgroundColor: "#64EABA" }}
                            >
                              <div className="inner">
                                <h4> Material Stock </h4>
                                <p>Inline</p>
                              </div>
                              <div className="icon">
                                <FcServices />
                              </div>
                              <Link to="/StockMat" className="small-box-footer">
                                More info{" "}
                                <i className="fas fa-arrow-circle-right" />
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                </div>
              </div>
            </section>
          </div>
        </div>
      </>
    );
  }
}
