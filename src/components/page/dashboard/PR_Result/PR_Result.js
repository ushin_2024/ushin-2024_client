import React, { Component } from "react";
import { Link } from "react-router-dom";
import { httpClient } from "../../../../utils/HttpClient";
import { OK, server } from "../../../../constance/constance";
import moment from 'moment'
import Swal from "sweetalert2";
import DatePicker from "react-datepicker";

export default class PR_Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Prod_date: moment().add(-0, "days").format("YYYY-MM-DD"),
      ProdData: [],
      table_display: "flex",
      edit_display: "none",

      startdate: moment().add(-0, "days").format("YYYY-MM-DD"),
      enddate: moment().add(-0, "days").format("YYYY-MM-DD"),
      Data: [],DueDate : "" , MONo :"", ReformattedDate: "" ,
    
    
    }
  }
  // componentDidMount = async () => {
  //   await this.getProd();
  // }
  getProd = async (e) => {
    // e.preventDefault(); 
    let Proddata = await httpClient.post(server.GetProductionResultLatest, {
      startdate: this.state.startdate,
      enddate: this.state.enddate
    })
    await this.setState({ ProdData: Proddata.data })
    console.log(this.state.ProdData);
    if (Proddata.data.length === 0) {
      await Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Can not find data',
        showConfirmButton: false,
      })
    } else {
      this.renderTB()
    }
  }

  renderTB() {
    if (this.state.ProdData != null) {
      return this.state.ProdData.map((item) =>
        <tr>
          <td>{moment(item.DueDate).format('DD-MMM-YYYY')}</td>
          <td> {item.DONo}</td>
          <td> {item.SuppName}</td>
          <td> {item.PartName}</td>
          <td> {item.Taget}</td>
          <td style={{ backgroundColor: item.SumOutput == item.Taget ? "#36EB62" : "transparent" }}> {item.SumOutput}</td>

        </tr>
      )
    }
  }
  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1 style={{ fontSize: "30px" }}>Production Result</h1>
                  </div>
                </div>
              </div>
            </div>
            <div className="container-fluid">
              <div className="col-md-12">
                <div className="card card-primary">
                  <div className="card-header"></div>
                  <div className="card-body">
                    <div className="col-12">
                      <br />
                      <form>
                        <div className="row" >
                          <br />
                          <div className="col-sm-2 form-group">  </div>
                          <div className="col-sm-3 form-group">
                            <input className="form-control" type="date"
                              id="id_daydate" name="name_daydate"
                              onChange={async (e) => {
                                await this.setState({
                                  startdate: moment(e.target.value).format("DD-MM-YYYY"),
                                });
                              }}
                            />
                          </div>
                          <div className="col-sm-3 form-group">
                            <input className="form-control" type="date" format="DD-MM-YYYY"
                              id="id_daydate"
                              onChange={async (e) => {
                                await this.setState({
                                  enddate: moment(e.target.value).format("DD-MM-YYYY"),
                                });
                              }}
                            />
                          </div>
                          <div className="col-sm-4 form-group">
                            <button type="submit" className="btn btn-success"
                              onClick={async (e) => {
                                e.preventDefault()
                                await this.getProd()
                                this.setState({
                                  edit_display: "flex",
                                  table_display: "none"
                                })
                              }}
                            >Search</button>
                          </div>
                        </div>
                      </form>


                    </div>
                  </div>
                  <div className="card" style={{ display: this.state.edit_display }}>
                    <div className="card-header">
                      <h2 className="card-title" style={{ fontSize: '18px' }}>Output</h2>
                    </div>
                    <div className="card-body">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>DueDate</th>
                            <th>DONo</th>
                            <th>SuppName</th>
                            <th>PartName</th>
                            <th>Target</th>
                            <th>SumOutput</th>
                          </tr>
                        </thead>
                        <tbody style={{ padding: '0px', fontSize: '12px' }}> </tbody>
                      </table>
                    </div>

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </>
    );
  }
}
