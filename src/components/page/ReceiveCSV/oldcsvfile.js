import { useState } from "react";
import "react-circular-progressbar/dist/styles.css";
import { httpClient } from "../../../utils/HttpClient";
import { server } from "../../../constance/constance";
import Swal from "sweetalert2";

function Oldcsvfile() {
  const [file, setFile] = useState("");

  const handleFileInputChange = (event) => {
    console.log(event.target.files);
    const file = event.target.files[0];
    setFile(file);
    console.log(file)
    const label = document.querySelector(".custom-file-label");
    if (label) {
      label.textContent = file ? file.name : "Choose file";
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append("file", file);
    try {
      let res = await httpClient.post(server.RECEIVECSVFILE_OPUCUS, formData);
      console.log(res);
      Swal.fire({
        icon: "success",
        title: "Upload Successful",
        text: "File uploaded successfully!",
      });
    } catch (error) {
      console.error(error);
      Swal.fire({
        icon: "error",
        title: "Upload Failed",
        text: "An error occurred while uploading the file.",
      });
    }
  };
  return (
 <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        margin: "40px",
        
      }}
    >
      <div className="container" style={{height:"30rem"}}>
        <div className="card card-primary">
          <div className="card-header">
          
            <h3 className="card-title">Upload CSV</h3>
          </div>
          <form onSubmit={handleSubmit}>
            <div className="card-body">
              <div className="form-group">
                <label htmlFor="exampleInputFile">File input</label>
                <div className="input-group">
                  <div className="custom-file">
                    <input
                      type="file"
                      className="custom-file-input"
                      id="exampleInputFile"
                      onChange={handleFileInputChange}
                    />
                    <label
                      className="custom-file-label"
                      htmlFor="exampleInputFile"
                    >
                      Choose file
                    </label>
                  </div>
                  <div className="input-group-append">
                    <button className="input-group-text" type="submit">
                      Upload
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Oldcsvfile;
