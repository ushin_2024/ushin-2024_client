import React, { Component } from "react";
import ReactDOM from "react-dom";
import * as XLSX from "xlsx";
import { httpClient } from "../../../utils/HttpClient";
import { server } from "../../../constance/constance";
import { RotatingLines } from "react-loader-spinner";
import Swal from "sweetalert2";

class CSVTest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //Excel
      file: {},
      fileBOM: {},
      loading: false,
    };
    this.handleFile = this.handleFile.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleFileBOM = this.handleFileBOM.bind(this);
    this.handleChangeBOM = this.handleChangeBOM.bind(this);
  }

  //Excel
  handleChange(e) {
    const files = e.target.files;
    if (files && files[0]) {
      document.getElementById("fileLable").innerHTML = e.target.files[0].name;
      this.setState({ file: files[0] });
    }
  }

  handleChangeBOM(e) {
    const files = e.target.files;
    if (files && files[0]) {
      document.getElementById("fileLableBOM").innerHTML = e.target.files[0].name;
      this.setState({ fileBOM: files[0] });
    }
  }

  handleFile() {
    
    Swal.fire({
      title: "Uploading...",
      html: '<div id="loader" style="display: flex; justify-content: center; align-items: center; height: 100px; overflow: hidden;"></div>',
      allowOutsideClick: false,
      showConfirmButton: false,
      didOpen: () => {
        const loader = document.getElementById("loader");
        ReactDOM.render(
          <RotatingLines color="#00BFFF" height={100} width={100} />,
          loader
        );
      },
    });

 /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;

    reader.onload = async (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      console.log(wb);
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
       /* Convert array of arrays */
      let data = XLSX.utils.sheet_to_json(ws, { raw: false });
      console.log(data);

      data = data.map((item) => ({
         // edit format date
        // key 'Delivery_Due' and 'Due_Date']
        ...item,
        Delivery_Due: formatDate(item.Delivery_Due),
        Due_Date: formatDate(item.Due_Date),
      }));

      try {
        const res = await httpClient.post(server.RECEIVECSVFILE_OPUCUS, data);
        Swal.close();
        this.setState({ loading: false });
        console.log(res.data);

        if (res.data === "OK") {
          Swal.fire({
            icon: "success",
            title: "Upload Successful",
            text: "File uploaded successfully!",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Upload Failed",
            text: "An error occurred while uploading the file.",
          });
        }
      } catch (error) {
        Swal.close();
        Swal.fire({
          icon: "error",
          title: "Upload Failed",
          text: "An error occurred while uploading the file.",
        });
      }
    };

    function formatDate(data) {
      if (data && !isNaN(new Date(data))) {
        const date = new Date(data);
        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        return `${month < 10 ? "0" + month : month}/${day < 10 ? "0" + day : day}/${year}`;
      } else {
        return data;
      }
    }

    if (rABS) {
      reader.readAsBinaryString(this.state.file);
    } else {
      reader.readAsArrayBuffer(this.state.file);
    }
  }

  handleFileBOM() {
    Swal.fire({
      title: "Uploading...",
      html: '<div id="loader" style="display: flex; justify-content: center; align-items: center; height: 100px; overflow: hidden;"></div>',
      allowOutsideClick: false,
      showConfirmButton: false,
      didOpen: () => {
        const loader = document.getElementById("loader");
        ReactDOM.render(
          <RotatingLines color="#00BFFF" height={100} width={100} />,
          loader
        );
      },
    });
    /* Boilerplate to set up FileReader */
    const reader = new FileReader();
    const rABS = !!reader.readAsBinaryString;

    reader.onload = async (e) => {
      /* Parse data */
      const bstr = e.target.result;
      console.log(bstr);

      const wb = XLSX.read(bstr, { type: rABS ? "binary" : "array" });
      console.log(wb);
      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];

      /* Convert array of arrays */
      let dataBOM = XLSX.utils.sheet_to_json(ws, { header: 0 });
      console.log(dataBOM);
    
      try {
        const res = await httpClient.post(server.RECEIVECSVFILE_BOM, dataBOM);
        console.log(res.dataBOM);
        Swal.close();
        this.setState({ loading: false });
        console.log(res.dataBOM);
        
        if (res.data === "OK") {
          Swal.fire({
            icon: "success",
            title: "Upload Successful",
            text: "File uploaded successfully!",
          });
        } else {
          Swal.fire({
            icon: "error",
            title: "Upload Failed",
            text: "An error occurred while uploading the file.",
          });
        }
      } catch (error) {
        Swal.close();
        Swal.fire({
          icon: "error",
          title: "Upload Failed",
          text: "An error occurred while uploading the file.",
        });
      }
    };

    if (rABS) {
      reader.readAsBinaryString(this.state.fileBOM);
    } else {
      reader.readAsArrayBuffer(this.state.fileBOM);
    }
  }

  renderUploadExcell = () => {
    const SheetJSFT = ["xlsx", "xlsb", "xlsm", "xls", "xml", "csv", "txt"]
      .map(x => "." + x)
      .join(",");

    return (
      <div className="wrapper">
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div className="col">
                <div className="card">
                  <div className="col-md-12">
                    <div className="card-header" style={{ fontSize: "30px"}}>
                      Upload File Summary of Production order
                    </div>
                    <div className="card-body">
                      <div className="card card-primary">
                        <div className="card-header">
                          <h1 className="card-title" >Upload CSV</h1>
                        </div>
                        <div className="card-body">
                          <div className="form-group" style={{ margin: "30px 30px 30px 30px" }}>
                            <div className="input-group">
                              <div className="custom-file">
                                <input
                                  type="file"
                                  className="form-control"
                                  id="file"
                                  accept={SheetJSFT}
                                  onChange={this.handleChange}
                                />
                                <label className="custom-file-label" id="fileLable" htmlFor="file">Choose file</label>
                              </div>
                              <div className="input-group-append" style={{ margin: "0 700px 0 0" }}>
                                <button type="submit" className="btn btn-primary" style={{padding : "2px 20px 0 20px"}} onClick={this.handleFile}>Upload</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
                <div className="col">
                <div className="card">
                  <div className="col-md-12">
                    <div className="card-header" style={{ fontSize: "30px"}}>
                      Upload File BOM
                    </div>
                    <div className="card-body">
                      <div className="card card-warning">
                        <div className="card-header">
                          <h1 className="card-title" >Upload CSV</h1>
                        </div>
                        <div className="card-body">
                          <div className="form-group" style={{ margin: "30px 30px 30px 30px" }}>
                            <div className="input-group">
                              <div className="custom-file">
                                <input
                                  type="file"
                                  className="form-control"
                                  id="fileBOM"
                                  accept={SheetJSFT}
                                  onChange={this.handleChangeBOM}
                                />
                                <label className="custom-file-label" id="fileLableBOM" htmlFor="fileBOM">Choose file</label>
                              </div>
                              <div className="input-group-append" style={{ margin: "0 700px 0 0" }}>
                                <button type="submit" style={{padding : "2px 20px 0 20px"}} className="btn btn-primary" onClick={this.handleFileBOM}>Upload</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </section>
       
      </div>
    );
  };

  render() {
    return (
      <>
        {this.state.loading && (
          <div className="loader-overlay">
            <RotatingLines visible={true} height="100" width="100" color="grey" strokeWidth="5" animationDuration="0.75" ariaLabel="rotating-lines-loading" wrapperStyle={{}} wrapperClass="" />
          </div>
        )}
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1 className="m-0" >Upload CSV File</h1>
                  </div>
                  <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right"></ol>
                  </div>
                </div>
              </div>
            </div>
            <section className="content">{this.renderUploadExcell()}</section>
          </div>
        </div>
      </>
    );
  }
}

export default CSVTest;
