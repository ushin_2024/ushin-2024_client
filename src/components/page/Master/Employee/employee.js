import { Link } from "react-router-dom";
import "./employee.css";
import React, { Component } from "react";
import { httpClient } from "../../../../utils/HttpClient";
import { server } from "../../../../constance/constance";
import Swal from "sweetalert2";
import { OK } from "../../../../constance/constance";

export default class employee extends Component {
  constructor(props) {
    super(props);

    this.state = {
      EmpData: [],
      EmployeeId: "",
      EmployeeName: "",
      RoleId: "",
      RoleId_select: [],
      list_Role: [],
      RoleName: "",
    };
  }
  componentDidMount = async () => {
    await this.getEmp();
  };
  refreshPage() {
    window.location.reload();
  }

  getEmp = async () => {
    const EmpData = await httpClient.get(server.getEmployee);
    const RoleData = await httpClient.get(server.getRole);
    this.setState({ list_Role: RoleData.data, EmpData: EmpData.data });
    console.log("Role:", this.state.list_Role, this.state.EmpData);
  };
  Table() {
    if (this.state.EmpData != null) {
      return this.state.EmpData.map((item) => (
        <tr>
          <td>{item.EmployeeId}</td>
          <td>{item.EmployeeName} </td>
          <td>{item["EmployeeRole.RoleName"]}</td>
        </tr>
      ));
    } else {
      Swal.fire({
        title: "The Internet?",
        text: "That thing is still around?",
        icon: "question",
      });
    }
  }
  TableRole() {
    if (this.state.list_Role != null) {
      return this.state.list_Role.map((item) => (
        <tr>
          <td>{item.RoleId}</td>
          <td>{item.RoleName} </td>
        </tr>
      ));
    }
  }
  renderRole = () => {
    try {
      if (this.state.list_Role !== null) {
        const myResult = this.state.list_Role;
        return myResult.map((item) => <option>{item.RoleName}</option>);
      }
    } catch (error) {
      Swal.fire({
        title: "Error",
        text: "Something wrong ........",
        icon: "error",
      });
    }
  };
  insertEmp = async () => {
    const insertEmp = await httpClient.post(server.insertEmployee, {
      EmployeeId: this.state.EmployeeId,
      EmployeeName: this.state.EmployeeName,
      RoleName: this.state.RoleId_select,
    });
    if (insertEmp.data == OK) {
      Swal.fire({
        title: "sucess",
        text: "Save Success",
        icon: "success",
      });
    } else {
      Swal.fire({
        title: "Can not input Emp",
        text: "Check for duplicate data !",
        icon: "error",
      });
    }
    this.refreshPage();
    console.log(insertEmp.data);
  };
  insert_Role = async () => {
    const insertRoleName = await httpClient.post(server.insertEmployeeRole, {
      RoleName: this.state.RoleName,
    });
    if (insertRoleName.data == OK) {
      Swal.fire({
        title: "sucess",
        text: "Save Success",
        icon: "success",
      });
    } else {
      Swal.fire({
        title: "Can not input Emp",
        text: "Check for duplicate data !",
        icon: "error",
      });
    }
    this.refreshPage();
  };
  render() {
    return (
      <div>
        <div className="wrapper">
          <div className="content-wrapper">
          <div className="content-header"></div>
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                <div className="col-6">
                  <div className="card card-warning card-outline">
                    <div className="card-header">
                      <h3 className="card-title" style={{ fontSize: "30px"}}>Employee Master</h3>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-md-2" />
                        <div className="col-md-auto">
                          <div className="card-body">
                            <table className="table table-bordered">
                              <thead>
                                <tr>
                                  <th>EmployeeId</th>
                                  <th> Name </th>
                                  <th>RoleName</th>
                                </tr>
                              </thead>
                              <tbody>{this.Table()}</tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div className="row col-12">
                        <div className="col-md-2" />
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="exampleInputName">EmployeeId</label>
                            <input
                              onChange={async (e) => {
                                e.preventDefault();
                                await this.setState({
                                  EmployeeId: e.target.value.toUpperCase(),
                                });
                              }}
                              type="text"
                              className="form-control"
                              id="exampleInputtext"
                              placeholder="Input EMP ID"
                            />
                          </div>
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="exampleInputType">
                              Employee Name
                            </label>
                            <input
                              onChange={async (e) => {
                                e.preventDefault();
                                await this.setState({
                                  EmployeeName: e.target.value.toUpperCase(),
                                });
                              }}
                              type="text"
                              className="form-control"
                              id="exampleInputType"
                              placeholder="Input EMP Name"
                            />
                          </div>{" "}
                          {/* {this.state.EmployeeName} */}
                        </div>
                        <div className="col-md-3">
                          <div className="form-group">
                            <label htmlFor="exampleInputType">Role Name</label>
                            <div className="input-group">
                              <select
                                onChange={(e) => {
                                  this.setState({
                                    RoleId_select: e.target.value,
                                  });
                                }}
                                className="form-control"
                                value={this.state.RoleId_select}
                              >
                                {" "}
                                <option>Select</option>
                                {this.renderRole()}
                              </select>
                              {/* {this.state.RoleId_select} */}
                            </div>{" "}
                          </div>
                        </div>
                      </div>
                      <div className="row col-12">
                        <div className="col-md-5" />
                        <div className="col-md-2" style={{margin : "10px 0 20px 0"}}>
                          <button
                            onClick={(e) => {
                              e.preventDefault();
                              this.insertEmp();
                            }}
                            type="submit"
                            className="btn btn-primary"
                          >
                            {" "}
                            <i className=" fas fa-download"></i> &nbsp; Add{" "}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="card card-warning card-outline">
                    <div className="card-header">
                      <h3 className="card-title" style={{ fontSize: "30px"}}>Employee Role</h3>
                    </div>
                    <div className="card-body">
                      <div className="row">
                        <div className="col-md-4" />
                        <div className="col-md-auto">
                          <div className="card-body">
                            <table className="table table-bordered">
                              <thead>
                                <tr>
                                  <th>RoleId</th>
                                  <th> RoleName </th>
                                </tr>
                              </thead>
                              <tbody> {this.TableRole()}</tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div className="row col-12">
                        <div className="col-md-3" />

                        <div className="col-md-5">
                          <div className="form-group">
                            <label htmlFor="exampleInputType">RoleName</label>
                            <input
                              onChange={(e) => {
                                this.setState({ RoleName: e.target.value });
                              }}
                              type="text"
                              className="form-control"
                              id="exampleInputType"
                              placeholder="Input RoleName"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row col-12">
                        <div className="col-md-5" />
                        <div className="col-md-2" style={{margin : "10px 0 20px 0"}}>
                          <button
                            type="submit"
                            className="btn btn-primary"
                            onClick={(e) => {
                              this.insert_Role();
                            }}
                          >
                            <i className=" fas fa-download"></i> &nbsp; Add{" "}
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}
