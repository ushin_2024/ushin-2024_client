import React, { Component } from "react";
import "./masterMC.css";
import { Link } from "react-router-dom";
import { httpClient } from "../../../../utils/HttpClient";
import { OK, server } from "../../../../constance/constance";
import Swal from "sweetalert2";

export default class masterMC extends Component {
  constructor(props) {
    super(props);

    this.state = {
      masterMC: [],
      Machine: "",
      MachineType: "",
      MachineName: "",
      MachineNumber: "",
    };
  }
  componentDidMount = () => {
    this.get_masterMC();
  };
  refreshPage() {
    window.location.reload();
  }
  get_masterMC = async () => {
    let data = await httpClient.get(server.getMasterMachine);
    this.setState({ masterMC: data.data });
  };
  renderTB() {
    if (this.state.masterMC != null) {
      return this.state.masterMC.map((item) => (
        <tr key={item.id}>
          <td>{item.MachineId}</td>
          <td>{item.Machine}</td>
          <td>{item.MachineType}</td>
          <td>{item.MachineName}</td>
          <td>{item.MachineNumber}</td>
        </tr>
      ));
    }
  }

  insertMC = async () => {
    const insertMC = await httpClient.post(server.insertMachine, {
      Machine: this.state.Machine,
      MachineType: this.state.MachineType,
      MachineNumber: this.state.MachineNumber,
      MachineName:
        this.state.Machine +
        "-" +
        this.state.MachineType +
        "-" +
        this.state.MachineNumber,
    });
    if (insertMC.data == OK) {
      Swal.fire({
        title: "success",
        text: "Save Success",
        icon: "success",
      });
    } else {
      Swal.fire({
        title: "Can not input Line",
        text: "Check for duplicate data !",
        icon: "error",
      });
    }
    this.refreshPage();
    console.log(insertMC.data);
  };
  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header">
              <div className="container-fluid">
                
              </div>
            </div>
            <div className="col-12">
              <div className="card card-primary card-outline">
                <div className="card-header">
                  <h3 className="card-title" style={{ fontSize: "30px"}} >Line Master</h3>
                </div>
                <div className="card-body">
                  <div className="row col-12">
                    <div className="col-md-1" />
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="exampleInputName">LineName</label>
                        <input
                          onChange={async (e) => { e.preventDefault();
                            await this.setState({
                              Machine: e.target.value,
                            });
                          }}
                          type="text"
                          className="form-control"
                          id="exampleInputtext"
                          placeholder="Input LineName"
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="exampleInputType">LineType</label>
                        <input
                          onChange={async (e) => {
                            e.preventDefault();
                            await this.setState({
                              MachineType: e.target.value,
                            });
                          }}
                          type="text"
                          className="form-control"
                          id="exampleInputType"
                          placeholder="Input LineType"
                        />
                      </div>
                    </div>
                    <div className="col-md-3">
                      <div className="form-group">
                        <label htmlFor="exampleInputNO">LineNo</label>
                        <input
                          onChange={async (e) => {
                            e.preventDefault();
                            await this.setState({
                              MachineNumber: e.target.value,
                            });
                          }}
                          type="text"
                          className="form-control"
                          id="exampleInputNO"
                          placeholder="Input LineNo"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row col-12">
                    <div className="col-md-5" />
                    <div className="col-md-2">
                      <button
                        onClick={(e) => {
                          e.preventDefault();
                          this.insertMC();
                        }}
                        type="submit"
                        className="btn btn-primary"
                      >
                        {" "}
                        <i className=" fas fa-download"></i> Add{" "}
                      </button>
                    </div>
                  </div>
                  <div className="row" style={{ margin: "15px" }}>
                    <div className="col-md-2" />
                    <div className="col-md-8">
                      <div className="card-body">
                        <table className="table table-bordered">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Line</th>
                              <th>LineType</th>
                              <th>LineName</th>
                              <th>LineNo</th>
                            </tr>
                          </thead>
                          <tbody style={{ fontFamily: "sans-serif" }}>
                            {this.renderTB()}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div className="text-muted mt-3"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
