import React, { Component } from "react";
import { Link } from "react-router-dom";
import { httpClient } from "../../../../utils/HttpClient";
import { OK, server } from "../../../../constance/constance";
import moment from "moment";
export default class masterPOstatus extends Component {
  constructor(props) {
    super(props);

    this.state = {
      masterPoStatus: [],
    };
  }
  componentDidMount = () => {
    this.get_masterPOstatus();
  };
  get_masterPOstatus = async () => {
    let data = await httpClient.get(server.getMasterPOStatus);
    this.setState({ masterPoStatus: data.data });
    console.log(data.data);
  };
  renderTB_po() {
    if (this.state.masterPoStatus != null) {
      return this.state.masterPoStatus.map((item) => (
        <tr key={item.MasterPOStatusId}>
          <td>{item.MasterPOStatusId}</td>
          <td>{item.POStatus}</td>
          <td>{moment(item.updatedAt).format("DD-MMM-YYYY")}</td>
        </tr>
      ));
    }
  }
  render() {
    return (
      <>
        <div className="wrapper">
          <div className="content-wrapper">
          <div className="content-header"></div>
            {/* <div className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1>Master PO Status</h1>
                  </div>
                  <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                      <li className="breadcrumb-item">
                        <Link to="/home">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">PO Status</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div> */}
            <div className="col-6">
              <div className="card card-primary card-outline">
                <div className="card-header">
                  <h3 className="card-title" style={{ fontSize: "30px"}}>Machine Master</h3>
                </div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-md-1" />
                    <div className="col-md-5">
                      <div className="form-group">
                        <label htmlFor="exampleInputNO">PO Status</label>
                        <input
                          type="text"
                          className="form-control"
                          id="exampleInputNO"
                          placeholder="input PO Status"
                        />
                      </div>
                    </div>
                    <div className="col-md-4">
                      <br />
                      <button type="submit" className="btn btn-primary">
                        {" "}
                        <i className=" fas fa-download"></i> Add{" "}
                      </button>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-11">
                      <div
                        className="card-body table-responsive p-0"
                        style={{ height: 500, textAlign: "center" }}
                      >
                        <table className="table table-head-fixed text-nowrap">
                          <thead>
                            <tr>
                              <th
                                style={{
                                  textAlign: "center",
                                  fontFamily: "sans-serif",
                                }}
                              >
                                {" "}
                                Id
                              </th>
                              <th>POStatus</th>
                              <th>updatedAt</th>
                            </tr>
                          </thead>
                          <tbody
                            style={{
                              textAlign: "center",
                              fontFamily: "sans-serif",
                            }}
                          >
                            {" "}
                            {this.renderTB_po()}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                  {/* <div className='row col-12'>
                                <div className="col-md-5" />
                                <div className="col-md-2" >
                                    <button type="submit" className="btn btn-primary" > <i className=' fas fa-download'></i> เพิ่ม</button>
                                </div>
                            </div> */}
                  <div className="text-muted mt-3"></div>
                </div>
              </div>
            </div>
            <div className="col-6"></div>
          </div>
        </div>
      </>
    );
  }
}
