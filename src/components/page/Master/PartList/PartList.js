import { Link } from "react-router-dom";
import React, { Component } from "react";
import { httpClient } from "../../../../utils/HttpClient";
import { OK, server } from "../../../../constance/constance";
import Swal from "sweetalert2";

export default class PartList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      PartList: "",
      groupPartList: [],
      PartNo: [],
    };
  }
  componentDidMount = async () => {
    await this.get_PartList();
  }; 
  refreshPage() {
    window.location.reload();
}

  get_PartList = async () => {
    let PartList_data = await httpClient.get(server.getPartList);
    await this.setState({ PartNo: PartList_data.data });
    // console.log(PartList_data.data)
  };
  renderTable_PartList() {
    if (this.state.PartNo != null) {
      return this.state.PartNo.map((item) =>
        <tr key={item.id}>
          <td>{item.PartNoId}</td>
          <td>{item.PartNo}</td>
        </tr>
      )
    }
  }
  insertPartList = async () => {
    let insertPartList = await httpClient.post(server.insertPartList, {
      PartNo: this.state.PartList,
    })
    if (insertPartList.data == OK) {
      Swal.fire({
        title: "sucess",
        text: "Save Success",
        icon: "success",
      });
    } else {
      Swal.fire({
        title: "Can not input Emp",
        text: "Check for duplicate data !",
        icon: "error",
      });
    }
    this.refreshPage();
    console.log(insertPartList.data);


  }

  render() {
    return (
      <div>
        <div className="wrapper">
          <div className="content-wrapper">
            <div className="content-header"></div>
            <section className="content">
              {/* <div className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6">
                    <h1>Part List(Master)</h1>
                  </div>
                  <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                      <li className="breadcrumb-item">
                        <Link to="/home">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">PartList</li>
                    </ol>
                  </div>
                </div>
              </div>
            </div> */}
              <div className="container-fluid">
                <div className="row">
                  <div className="col-12">
                    <div className="card card-success card-outline">
                      <div className="card-header">
                        <h3 className="card-title" style={{ fontSize: "30px"}}>Part List Master</h3>
                      </div>
                      <div className="card-body">
                        <div className="row">
                          <div className="row col-12">
                            <div className="col-md-3" />
                            <div className="col-md-4">
                              <div className="form-group">
                                <label htmlFor="exampleInputName">PartNo</label>
                                <input 
                                       onChange={async (e) => {
                                        e.preventDefault();
                                        await this.setState({
                                            PartList: e.target.value,
                                        });
                                    }}
                                  type="text"
                                  className="form-control"
                                  id="exampleInputtext"
                                  placeholder="input PartNo"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="row col-12">
                            <div className="col-md-5" />
                            <div className="col-md-2">
                              <button  
                              onClick={(e) => { 
                                e.preventDefault();
                                this.insertPartList();
                              }}
                              type="submit" className="btn btn-primary">
                        
                                <i className=" fas fa-download"></i> &nbsp; Add
                              </button>
                            </div>
                          </div>
                        </div>
                        <div className="row" style={{ margin: "15px" }}>
                          <div className="col-md-2" />
                          <div className="col-8">
                            <div className="card">
                              <div className="card-header">
                                <h3 className="card-title"></h3>
                              </div>
                              <div
                                className="card-body table-responsive p-0"
                                style={{
                                  fontFamily: "sans-serif",
                                  height: 300,
                                  textAlign: "center",
                                }}
                              >
                                <table className="table table-head-fixed text-nowrap">
                                  <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>PartNo</th>
                                    </tr>
                                  </thead>
                                  <tbody style={{ fontFamily: "sans-serif" }}>
                                    {this.renderTable_PartList()}
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    );
  }
}
