import React, { Component } from 'react'
export default class login extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         empNo :'',
         
      }
    }

    render() { 
        return (
            <div className="wrapper">
                <div className='login-page'>
                    <div className="login-box">
                        <div className="login-logo">
                            <a href="../../index2.html"><b>Sign-In</b></a>
                        </div>
                        <div className="card">
                            <div className="card-body login-card-body">
                                <p className="login-box-msg">Sign in to start your session</p>
                                <form action="../../index3.html" method="post">
                                    <div className="input-group mb-3">
                                        <input type="text" className="form-control" placeholder="Emp No" />
                                        <div className="input-group-append">
                                            <div className="input-group-text">
                                                <span className="fas fa-user" />
                                            </div>
                                        </div>
                                    </div>
                                 
                                    <div className="row">
                                        <div className="col-8">
                                           
                                        </div>
                                        <div className="col-4">
                                            <button type="submit" className="btn btn-primary btn-block">Sign In</button>
                                        </div>
                                    </div>
                                </form>
                        
                                <p className="mb-1">
                                    <a href="forgot-password.html">I forgot my password</a>
                                </p>
                                <p className="mb-0">
                                    <a href="register.html" className="text-center">Register a new membership</a>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        )
    }
}
