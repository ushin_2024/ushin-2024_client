import React, { Component } from "react";

export default class footer extends Component {
  render() {
    return (
      <footer
        className="main-footer"
        style={{
          backgroundColor: "#0097a7",
          color: "black",
          borderRadius: "0.2rem",
        }}
      >
        <strong>
          Manufacturing EXECUTION System &copy; 2024{" "}
          <a style={{ color: "blue" }}>Developed by MIC DIV. </a>{" "}
          {/*Traceability Systems*/}
        </strong>
        {/* All rights reserved. */}
        <div className="float-right d-none d-sm-inline-block">
          <b>Version</b> 1.0.0
        </div>
      </footer>
    );
  }
}
