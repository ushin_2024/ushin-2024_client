import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class side extends Component {
  render() {
    return (
      <aside
        className="main-sidebar sidebar-info elevation-4"
        style={{ backgroundColor: "#c1ddea", borderRadius: "0.2rem" }}
      >
        <Link to="/home.js" className="brand-link">
          <img
            src="dist/img/logo.png"
            alt="AdminLTE Logo"
            className="brand-image img-squre elevation-3"
            style={{ opacity: "1" }}
          />
          <span className="brand-text font-weight-dark"> U-SHIN </span>
        </Link>
        <div className="sidebar os-host os-theme-dark os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-transition os-host-scrollbar-horizontal-hidden">
          <div className="os-resize-observer-host observed">
            <div
              className="os-resize-observer"
              style={{ left: 0, right: "auto" }}
            />
          </div>
          <div
            className="os-size-auto-observer observed"
            style={{ height: "calc(100% + 1px)", float: "left" }}
          >
            <div className="os-resize-observer" />
          </div>
          <div
            className="os-content-glue"
            style={{ margin: "0px -8px", width: 249, height: 767 }}
          />
          <div className="os-padding">
            <div
              className="os-viewport os-viewport-native-scrollbars-invisible"
              style={{ overflowY: "scroll" }}
            >
              <div
                className="os-content"
                style={{ padding: "0px 8px", height: "100%", width: "100%" }}
              >
                <nav className="mt-2">
                  <ul
                    className="nav nav-pills nav-sidebar flex-column"
                    data-widget="treeview"
                    role="menu"
                    data-accordion="false"
                  >
                    <li className="nav-item menu-open">
                      <Link
                        to="#"
                        className="nav-link active"
                        style={{ borderRadius: "2rem" }}
                      >
                        <i className="nav-icon fas fa-edit" />
                        <p>
                          Record
                          <i className="right fas fa-angle-left" />
                        </p>
                      </Link>
                      <ul className="nav nav-treeview">
                      <li className="nav-item">
                          <Link to="/PR_scheduling" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Production scheduling</p>
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/MaterialRequisition" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Material Requisition</p>
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/home" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Record Data</p>
                          </Link>
                        </li>
                      </ul>
                    </li>
                    <li className="nav-item menu-open">
                      <Link
                        to="/DashBoard"
                        className="nav-link active"
                        style={{ borderRadius: "2rem" }}
                      >
                        <i className="nav-icon fas fa-tachometer-alt" />
                        <p>
                          Dashboard
                          {/* <i className="right fas fa-angle-left" /> */}
                        </p>
                      </Link>
                      {/* <ul className="nav nav-treeview">
                        <li className="nav-item">
                          <Link to="/DashBoard" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Dashboard</p>
                          </Link>
                        </li>
                      </ul> */}
                    </li>
                    <li className="nav-item menu-open">
                      <Link
                        to="#"
                        className="nav-link active"
                        style={{ borderRadius: "2rem" }}
                      >
                        <i className="nav-icon fas fa-table" />
                        <p>
                          Admin
                          <i className="fas fa-angle-left right" />
                        </p>
                      </Link>
                      <ul
                        className="nav nav-treeview"
                        style={{ display: "block" }}
                      >
                        <li className="nav-item">
                          <Link to="/EditDO" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Delete DO/PO</p>
                          </Link>
                        </li>
                        
                        <li className="nav-item">
                          <Link to="/Uploadfile" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Upload CSV File</p>
                          </Link>
                        </li>

                        {/* <li className="nav-item">
                          <Link to="/partList" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>PartList</p>
                          </Link>
                        </li> */}
                        <li className="nav-item">
                          <Link to="/masterMC" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Line </p>
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/MaterialControl" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Material Control</p>
                          </Link>
                        </li>
                        <li className="nav-item">
                          <Link to="/emp_master" className="nav-link">
                            <i className="far fa-circle nav-icon" />
                            <p>Employee</p>
                          </Link>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <div className="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden">
            <div className="os-scrollbar-track">
              <div
                className="os-scrollbar-handle"
                style={{ width: "100%", transform: "translate(0px, 0px)" }}
              />
            </div>
          </div>
          <div className="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden">
            <div className="os-scrollbar-track">
              <div
                className="os-scrollbar-handle"
                style={{
                  height: "38.2661%",
                  transform: "translate(0px, 186.962px)",
                }}
              />
            </div>
          </div>
          <div className="os-scrollbar-corner" />
        </div>
      </aside>
    );
  }
}
