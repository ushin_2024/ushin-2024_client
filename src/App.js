import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";

import Header from "./components/layout/header.js";
import Side from "./components/layout/site.js";
import Footer from "./components/layout/footer.js";
import Home from "./components/page/home/home.js";
import MasterMC from "./components/page/Master/Machine/masterMC";
import MasterPOstatus from "./components/page/Master/POStatus/masterPOstatus.js";
import DO_Data from "./components/page/DO_Data/DO_data";
import Emp_master from "./components/page/Master/Employee/employee";
import PartList from "./components/page/Master/PartList/PartList.js";
import ChildPartList from "./components/page/ChildPartList/ChildPartList";
import WIP_output from "./components/page/WIP_output/WIP_output";
import DashBoard from "./components/page/dashboard/dashHome";
import ProdData from "./components/page/dashboard/PR_Result/PR_Result";
import StockMat from "./components/page/dashboard/StockMat/StockMat";
import SummarizeOutput from "./components/page/dashboard/SummarizeOutput/SummarizeOutput";
// import ReceiveMat from "./components/page/ReceiveMat/receiveMat.js";
import ShopperRecive from './components/page/Shopper/ShopperRecive.js'
// import Traceability from "./components/page/Traceability/Traceability.js";
import MaterialRequisition from "./components/page/MRdata_request/MRdata_request"
import Losepage from './components/page/Losepage/Losepage';
import MOData_print from "./components/page/MOData_print/MOData_print";
import PR_scheduling from "./components/page/Production_scheduling/Production_scheduling"
// import Shopper from "./components/page/Shopper/Shopper.js";
// import Test from "./components/page/ReceiveCSV/Untitled-1.js"
// import TraceabilityTest from './components/page/Traceability/Traceability_test.js';
import CSVTest from './components/page/ReceiveCSV/CSVTest.js'
import Tab from "./components/page/Traceability/Traceability_Tab.js";
import EmployeeScan from "./components/page/EmployeeScan/EmployeeScan.js"
import MaterialControl from "./components/page/MaterialControl/MaterialControl.js";
import EditDO from "./components/page/EditDO/EditDO.js"


import Login from "./components/layout/login.js";

export default class App extends Component {
  redirectHome = () => {
    //ถ้า return error ให้ไปที่ login
    return <Navigate to="/home" />;
  };
  redirectToCreate = () => {
    return <Navigate to="/MaterialRequisition" />;
  };
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Side />
          <Routes>

            {/* <Route path="/header" element={<Header/>} /> */}
            {/* <Route path="/site" element={<Site/>} /> */}
            <Route path="/home" element={<Home />} />
            <Route path="/Login" element={<Login />} />
            <Route path="/masterMC" element={<MasterMC />} />
            <Route path="/masterPOstatus" element={<MasterPOstatus />} />
            <Route path="/DeliveryData" element={<DO_Data />} />
            <Route path="/emp_master" element={<Emp_master />} />
            <Route path="/partList" element={<PartList />} />
            <Route path="/ChildPartList" element={<ChildPartList />} />
            <Route path="/WIP_output" element={<WIP_output />} />
            <Route path="/DashBoard" element={<DashBoard />} />
            <Route path="/ProdData" element={<ProdData />} />
            <Route path="/StockMat" element={<StockMat />} />
            <Route path="/SummarizeOutput" element={<SummarizeOutput />} />
            <Route path="/ShopperRecive" element={<ShopperRecive />} />
            <Route path="/MaterialRequisition" element={<MaterialRequisition />} />
            <Route path="/PR_scheduling" element={<PR_scheduling />} />
            <Route path="/Losepage" element={<Losepage />} />
            <Route path="/MOData_print" element={<MOData_print />} />
            <Route path="/Traceability" element={<Tab />} />
            <Route path="/EmployeeScan" element={<EmployeeScan />} />
            <Route path="/MaterialControl" element={<MaterialControl />} />
            <Route path="/EditDO" element={<EditDO />} />
    


            <Route path="/Uploadfile" element={<CSVTest />} />
            <Route path="/" element={<this.redirectHome />} />
            <Route path="*" element={<this.redirectHome />} />

          </Routes>
          <Footer />
        </div>
      </Router>
    );
  }
}
